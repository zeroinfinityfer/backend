package hr.zeroinfinity.backend.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${app.documentation.basePackage}")
    private String BASE_PACKAGE;
    @Value("${app.documentation.title}")
    private String TITLE;
    @Value("${app.documentation.description}")
    private String DESCRIPTION;
    @Value("${app.documentation.version}")
    private String VERSION;
    @Value("${app.documentation.contact.name}")
    private String NAME;
    @Value("${app.documentation.contact.url}")
    private String URL;
    @Value("${app.documentation.contact.email}")
    private String EMAIL;
    @Value("${app.documentation.contact.license}")
    private String LICENSE;
    @Value("${app.documentation.contact.licenseURL}")
    private String LICENSE_URL;

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
                .paths(PathSelectors.any())
                .build().apiInfo(apiInfo());

    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                TITLE,
                DESCRIPTION,
                VERSION,
                "",
                new Contact(NAME, URL, EMAIL),
                LICENSE, LICENSE_URL, Collections.emptyList());
    }
}