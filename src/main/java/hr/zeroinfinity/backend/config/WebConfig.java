package hr.zeroinfinity.backend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.WebJarsResourceResolver;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    private static final String[] RESOURCE_HANDLERS = {
            "/webjars/**",
            "/img/**",
            "/css/**",
            "/js/**",
            "swagger-ui.html"
    };

    private static final String[] RESOURCE_LOCATIONS = {
            "classpath:/META-INF/resources/webjars/",
            "classpath:/static/img/",
            "classpath:/static/css/",
            "classpath:/static/js/",
            "classpath:/META-INF/resources/"
    };

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(
                RESOURCE_HANDLERS)
                .addResourceLocations(
                        RESOURCE_LOCATIONS)
                .resourceChain(true).addResolver(new WebJarsResourceResolver());
    }
}