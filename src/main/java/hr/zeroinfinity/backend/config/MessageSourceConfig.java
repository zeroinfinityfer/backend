package hr.zeroinfinity.backend.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Configuration
public class MessageSourceConfig {
    private final List<Locale> LOCALES = Arrays.asList(Locale.ENGLISH, Locale.forLanguageTag("hr"));

    {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:locale/messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
        bean.setValidationMessageSource(messageSource());
        return bean;
    }

    @Bean
    public LocaleResolver localeResolver() {
        return new AcceptHeaderLocaleResolver() {

            @Override
            public Locale resolveLocale(HttpServletRequest request) {
                String header = request.getHeader("Accept-Language");

                if (header == null || header.isEmpty()) {
                    return Locale.getDefault();
                }

                List<Locale.LanguageRange> list = Locale.LanguageRange.parse(header);
                Locale locale = Locale.lookup(list, LOCALES);

                return locale == null ? Locale.getDefault() : locale;
            }
        };
    }

}