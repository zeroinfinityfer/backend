package hr.zeroinfinity.backend.controllers.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "info")
public class InfoController {

    @Value("${app.version}")
    private String appVersion;

    @GetMapping(path = "/version", produces = "text/plain")
    public String getVersion(){
        return appVersion;
    }
}
