package hr.zeroinfinity.backend.controllers.rest;

import hr.zeroinfinity.backend.dto.ResponseDTO;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

public class AbstractController {

    private static final ResponseDTO VALID_RESPONSE = new ResponseDTO();
    private final MessageSource messageSource;
    protected final LocaleResolver localeResolver;
    private final ThreadLocal<ResponseDTO> cache;

    protected AbstractController(MessageSource messageSource, LocaleResolver localeResolver) {
        this.messageSource = messageSource;
        this.localeResolver = localeResolver;
        this.cache = new ThreadLocal<>();
    }

    protected ResponseDTO createResponseDTO(BindingResult bindingResult, HttpServletRequest request) {
        ResponseDTO responseDTO = getResponseObject();
        bindingResult.getAllErrors().forEach(error -> {
            FieldError fieldError = (FieldError) error;
            String fieldName = fieldError.getField();

            responseDTO.addError(fieldName, messageSource.getMessage(fieldError.getDefaultMessage(), null, localeResolver.resolveLocale(request)));
        });

        return responseDTO;
    }

    protected ResponseDTO createResponseDTO(Object id) {
        ResponseDTO responseDTO = getResponseObject();
        responseDTO.setEntityId(id);
        return responseDTO;
    }

    private ResponseDTO getResponseObject() {
        ResponseDTO responseDTO = cache.get();
        if (responseDTO == null) {
            responseDTO = new ResponseDTO();
            cache.set(responseDTO);
        }

        responseDTO.getErrors().clear();
        return responseDTO;
    }

    protected ResponseDTO createResponseDTO() {
        return VALID_RESPONSE;
    }

    protected Locale resolveLocale(HttpServletRequest request) {
        return localeResolver.resolveLocale(request);
    }
}
