package hr.zeroinfinity.backend.controllers.rest;

import hr.zeroinfinity.backend.dto.CalendarStatisticsDTO;
import hr.zeroinfinity.backend.dto.CategoryDTO;
import hr.zeroinfinity.backend.dto.DailyStatisticsDTO;
import hr.zeroinfinity.backend.dto.ExpenseDTO;
import hr.zeroinfinity.backend.dto.FinanceStatisticsDTO;
import hr.zeroinfinity.backend.dto.GoalDTO;
import hr.zeroinfinity.backend.dto.IncomeDTO;
import hr.zeroinfinity.backend.services.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "finance")
public class FinanceController extends AbstractController {

    private final FinanceService financeService;

    @Autowired
    public FinanceController(MessageSource messageSource, LocaleResolver localeResolver, FinanceService financeService) {
        super(messageSource, localeResolver);

        this.financeService = financeService;
    }

    @GetMapping(path = "/incomes", produces = "application/json")
    public ResponseEntity<Page<IncomeDTO>> getIncomesByGroupIdPageable(@RequestParam UUID groupId, @RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
        return ResponseEntity.ok(
                financeService.getIncomesByGroupIdPageable(
                        groupId, PageRequest.of(page, size, Sort.Direction.DESC, "dateCreated")));
    }

    @GetMapping(path = "/expenses", produces = "application/json")
    public ResponseEntity<Page<ExpenseDTO>> getExpensesByGroupIdPageable(@RequestParam UUID groupId, @RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
        return ResponseEntity.ok(
                financeService.getExpensesByGroupIdPageable(
                        groupId, PageRequest.of(page, size, Sort.Direction.DESC, "dateCreated")));
    }

    @GetMapping(path = "/goals", produces = "application/json")
    public ResponseEntity<Page<GoalDTO>> getGoalsByGroupIdPageable(@RequestParam UUID groupId, @RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "10") Integer size) {
        return ResponseEntity.ok(
                financeService.getGoalsByGroupIdPageable(
                        groupId, PageRequest.of(page, size, Sort.Direction.ASC, "targetDate")));
    }

    @GetMapping(path = "/categories", produces = "application/json")
    public ResponseEntity<List<CategoryDTO>> getCategoriesByGroupId(@RequestParam UUID groupId, HttpServletRequest request) {
        return ResponseEntity.ok(financeService.getCategoriesByGroupId(groupId, resolveLocale(request)));
    }

    @GetMapping(path = "/statistics", produces = "application/json")
    public ResponseEntity<FinanceStatisticsDTO> getOverallStatistics(@RequestParam UUID groupId, @RequestParam String periodUnit, @RequestParam Integer periodLength, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate) {
        return ResponseEntity.ok(financeService.getStatistics(groupId, periodUnit, periodLength, toDate));
    }

    @GetMapping(path = "/statistics/daily", produces = "application/json")
    public ResponseEntity<DailyStatisticsDTO> getDailyStatistics(@RequestParam UUID groupId, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date date){
        return ResponseEntity.ok(financeService.getDailyStatistics(groupId, date));
    }

    @GetMapping(path = "/statistics/calendar", produces = "application/json")
    public ResponseEntity<CalendarStatisticsDTO> getCalendarStatistics(@RequestParam UUID groupId, @RequestParam String periodUnit, @RequestParam Integer periodLength, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate) {
        return ResponseEntity.ok(financeService.getCalendarStatistics(groupId, periodUnit, periodLength, toDate));
    }
}
