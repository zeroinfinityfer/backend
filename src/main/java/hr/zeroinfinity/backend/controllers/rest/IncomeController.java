package hr.zeroinfinity.backend.controllers.rest;

import hr.zeroinfinity.backend.dto.IncomeDTO;
import hr.zeroinfinity.backend.dto.PairDTO;
import hr.zeroinfinity.backend.dto.ResponseDTO;
import hr.zeroinfinity.backend.services.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "income")
public class IncomeController extends AbstractController {

    private final FinanceService financeService;

    @Autowired
    public IncomeController(MessageSource messageSource, LocaleResolver localeResolver, FinanceService financeService) {
        super(messageSource, localeResolver);
        this.financeService = financeService;
    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerIncome(HttpServletRequest request, @RequestBody @Valid IncomeDTO incomeDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        Long entityId = financeService.registerIncome(incomeDTO);
        ResponseDTO response = createResponseDTO(entityId);

        return ResponseEntity.ok(response);
    }

    @PutMapping(path = "/edit", produces = "application/json")
    public ResponseEntity<ResponseDTO> editIncome(HttpServletRequest request, @RequestBody @Valid IncomeDTO incomeDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        if (incomeDTO.getId() == null) return ResponseEntity.unprocessableEntity().build();

        financeService.editIncome(incomeDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deleteIncome(@RequestParam @NotNull Long id) {
        financeService.deleteIncome(id);

        return ResponseEntity.ok(createResponseDTO());
    }

    @GetMapping(path = "/statistics", produces = "application/json")
    public ResponseEntity<List<PairDTO>> getStatistic(@RequestParam UUID groupId, @RequestParam String periodUnit, @RequestParam Integer periodLength, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate) {
        return ResponseEntity.ok(financeService.getIncomesStatistics(groupId, periodUnit, periodLength, toDate));
    }
}
