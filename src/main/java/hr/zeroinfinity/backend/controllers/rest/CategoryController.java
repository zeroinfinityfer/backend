package hr.zeroinfinity.backend.controllers.rest;

import hr.zeroinfinity.backend.dto.CategoryDTO;
import hr.zeroinfinity.backend.dto.ResponseDTO;
import hr.zeroinfinity.backend.services.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "category")
public class CategoryController extends AbstractController {

    private final FinanceService financeService;

    @Autowired
    public CategoryController(MessageSource messageSource, LocaleResolver localeResolver, FinanceService financeService) {
        super(messageSource, localeResolver);
        this.financeService = financeService;
    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerCategory(HttpServletRequest request, @RequestBody @Valid CategoryDTO categoryDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        Long entityId = financeService.registerCategory(categoryDTO);
        ResponseDTO response = createResponseDTO(entityId);

        return ResponseEntity.ok(response);
    }

    @PutMapping(path = "/edit", produces = "application/json")
    public ResponseEntity<ResponseDTO> editCategory(HttpServletRequest request, @RequestBody @Valid CategoryDTO categoryDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        if (categoryDTO.getId() == null) return ResponseEntity.unprocessableEntity().build();

        financeService.editCategory(categoryDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deleteCategory(@RequestParam Long id) {
        financeService.deleteCategory(id);

        return ResponseEntity.ok(createResponseDTO());
    }
}
