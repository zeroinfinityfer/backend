package hr.zeroinfinity.backend.controllers.rest;

import hr.zeroinfinity.backend.dto.DeviceSubscriptionDTO;
import hr.zeroinfinity.backend.dto.ResponseDTO;
import hr.zeroinfinity.backend.dto.UserDTO;
import hr.zeroinfinity.backend.dto.validators.UserDTOValidator;
import hr.zeroinfinity.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Email;

@RestController
@RequestMapping(path = "user")
public class UserController extends AbstractController {

    private UserService userService;
    private UserDTOValidator userDTOValidator;

    @Autowired
    public UserController(UserService userService, UserDTOValidator userDTOValidator, LocaleResolver localeResolver, MessageSource messageSource) {
        super(messageSource, localeResolver);

        this.userService = userService;
        this.userDTOValidator = userDTOValidator;
    }

    @GetMapping(path = "/identify", produces = "application/json")
    public ResponseEntity<ResponseDTO> identify() {
        return ResponseEntity.ok(createResponseDTO());
    }

    @GetMapping(path = "/email", produces = "application/json")
    public ResponseEntity<UserDTO> getUserByEmail(@RequestParam String email) {
        UserDTO user = userService.getUserByEmail(email);

        return ResponseEntity.ok(user);

    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerUser(@RequestBody @Valid UserDTO userDTO, BindingResult bindingResult, HttpServletRequest request) throws MessagingException {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(createResponseDTO(bindingResult, request));
        }

        userService.registerUser(userDTO, localeResolver.resolveLocale(request));

        return ResponseEntity.ok(createResponseDTO());
    }

    @GetMapping(path = "/forgotPassword", produces = "application/json")
    public ResponseEntity<ResponseDTO> resetPassword(@RequestParam(name = "email") @Email String email) throws MessagingException {

        userService.resetPassword(email);

        return ResponseEntity.ok(createResponseDTO());
    }

    @PutMapping(path = "/device/subscribe", produces = "application/json")
    public ResponseEntity<ResponseDTO> subscribeDevice(@RequestBody DeviceSubscriptionDTO deviceSubscriptionDTO) {
        if (deviceSubscriptionDTO.getOldToken() != null) {
            userService.removeDevice(deviceSubscriptionDTO.getOldToken());
        }

        userService.registerDevice(deviceSubscriptionDTO.getNewToken());
        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/device/unsubscribe", produces = "application/json")
    public ResponseEntity<ResponseDTO> unsubscribeDevices(@RequestParam String deviceId) {
        userService.removeDevice(deviceId);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PostMapping(path = "/personalGroup", produces = "application/json")
    public ResponseEntity<ResponseDTO> changePersonalGroupState(@RequestParam Boolean enabled){
        userService.changePersonalGroupState(enabled);
        return ResponseEntity.ok(createResponseDTO());
    }

    @InitBinder("userDTO")
    private void initUserDTOBinder(WebDataBinder binder) {
        binder.addValidators(userDTOValidator);
    }

}