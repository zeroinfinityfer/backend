package hr.zeroinfinity.backend.controllers.rest;

import hr.zeroinfinity.backend.dto.ExpenseDTO;
import hr.zeroinfinity.backend.dto.ResponseDTO;
import hr.zeroinfinity.backend.services.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(path = "expense")
public class ExpensesController extends AbstractController {

    private final FinanceService financeService;

    @Autowired
    public ExpensesController(MessageSource messageSource, LocaleResolver localeResolver, FinanceService financeService) {
        super(messageSource, localeResolver);

        this.financeService = financeService;
    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerExpense(HttpServletRequest request, @RequestBody @Valid ExpenseDTO expenseDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        Long entityId = financeService.registerExpense(expenseDTO);
        ResponseDTO response = createResponseDTO(entityId);

        return ResponseEntity.ok(response);
    }

    @PutMapping(path = "/edit", produces = "application/json")
    public ResponseEntity<ResponseDTO> editExpense(HttpServletRequest request, @RequestBody @Valid ExpenseDTO expenseDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        if (expenseDTO.getId() == null) return ResponseEntity.unprocessableEntity().build();

        financeService.editExpense(expenseDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deleteExpense(@RequestParam @NotNull Long id) {
        financeService.deleteExpense(id);

        return ResponseEntity.ok(createResponseDTO());
    }

    @GetMapping(path = "/statistics", produces = "application/json")
    public ResponseEntity<Map<String, Double>> getStatistic(@RequestParam UUID groupId, @RequestParam String periodUnit, @RequestParam Integer periodLength, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate, HttpServletRequest request) {
        return ResponseEntity.ok(financeService.getExpensesStatistics(groupId, periodUnit, periodLength, resolveLocale(request), toDate));
    }
}
