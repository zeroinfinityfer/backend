package hr.zeroinfinity.backend.controllers.rest;

import hr.zeroinfinity.backend.dto.GoalDTO;
import hr.zeroinfinity.backend.dto.ResponseDTO;
import hr.zeroinfinity.backend.services.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

@RestController
@RequestMapping(path = "goal")
public class GoalController extends AbstractController {

    private final FinanceService financeService;

    @Autowired
    public GoalController(MessageSource messageSource, LocaleResolver localeResolver, FinanceService financeService) {
        super(messageSource, localeResolver);
        this.financeService = financeService;
    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerGoal(HttpServletRequest request, @RequestBody @Valid GoalDTO goalDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        Long entityId = financeService.registerGoal(goalDTO);
        ResponseDTO response = createResponseDTO(entityId);

        return ResponseEntity.ok(response);
    }

    @PutMapping(path = "/edit", produces = "application/json")
    public ResponseEntity<ResponseDTO> editGoal(HttpServletRequest request, @RequestBody @Valid GoalDTO goalDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        if (goalDTO.getId() == null) return ResponseEntity.unprocessableEntity().build();

        financeService.editGoal(goalDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deleteGoal(@RequestParam @NotNull Long id) {
        financeService.deleteGoal(id);

        return ResponseEntity.ok(createResponseDTO());
    }

    @PutMapping(path = "/approve", produces = "application/json")
    public ResponseEntity<ResponseDTO> approveGoal(@RequestParam @Valid Long goalId, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date today, HttpServletRequest request) {
        Boolean approve = financeService.approveGoal(goalId, resolveLocale(request), today);
        ResponseDTO response = createResponseDTO();

        if (!approve) {
            response.setValid(false);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
        }

        return ResponseEntity.ok(response);
    }
}
