package hr.zeroinfinity.backend.controllers.rest;

import hr.zeroinfinity.backend.dto.ResponseDTO;
import hr.zeroinfinity.backend.dto.SavingEntryDTO;
import hr.zeroinfinity.backend.dto.SavingStatisticsDTO;
import hr.zeroinfinity.backend.services.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping(path = "savings")
public class SavingsController extends AbstractController {

    private final FinanceService financeService;

    @Autowired
    protected SavingsController(MessageSource messageSource, LocaleResolver localeResolver, FinanceService financeService) {
        super(messageSource, localeResolver);
        this.financeService = financeService;
    }

    @PostMapping(path = "/addTo", produces = "application/json")
    public ResponseEntity<ResponseDTO> addToSaving(HttpServletRequest request,@RequestBody @Valid SavingEntryDTO savingEntry,  BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        financeService.registerSavingEntry(savingEntry);

        return ResponseEntity.ok(createResponseDTO());
    }

    @GetMapping(path = "/statistics", produces = "application/json")
    public ResponseEntity<SavingStatisticsDTO> getSavingStatistics(@RequestParam UUID groupId, @RequestParam String periodUnit, @RequestParam Integer periodLength, @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date toDate) {
        return ResponseEntity.ok(financeService.getSavingEntryStatistics(groupId, periodUnit, periodLength, toDate));
    }
}
