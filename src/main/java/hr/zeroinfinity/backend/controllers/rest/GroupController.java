package hr.zeroinfinity.backend.controllers.rest;

import hr.zeroinfinity.backend.dto.GroupAdminDTO;
import hr.zeroinfinity.backend.dto.GroupAdminRemoveDTO;
import hr.zeroinfinity.backend.dto.GroupDTO;
import hr.zeroinfinity.backend.dto.GroupInviteDTO;
import hr.zeroinfinity.backend.dto.GroupMemberInviteDTO;
import hr.zeroinfinity.backend.dto.GroupMemberRemoveDTO;
import hr.zeroinfinity.backend.dto.InviteDTO;
import hr.zeroinfinity.backend.dto.ResponseDTO;
import hr.zeroinfinity.backend.dto.validators.GroupDTOValidator;
import hr.zeroinfinity.backend.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "group")
public class GroupController extends AbstractController {

    private final GroupService groupService;
    private final GroupDTOValidator groupDTOValidator;

    @Autowired
    public GroupController(GroupService groupService, GroupDTOValidator groupDTOValidator,
                           LocaleResolver localeResolver, MessageSource messageSource) {
        super(messageSource, localeResolver);

        this.groupService = groupService;
        this.groupDTOValidator = groupDTOValidator;
    }

    @GetMapping(path = {"", "/"}, produces = "application/json")
    public ResponseEntity<List<GroupDTO>> getGroups(HttpServletRequest request) {
        return ResponseEntity.ok(groupService.getGroupsOfCurrentUser(resolveLocale(request)));
    }

    @GetMapping(path = "/invite", produces = "application/json")
    public ResponseEntity<List<InviteDTO>> getInvites() {
        return ResponseEntity.ok(groupService.getInvitesOfCurrentUser());
    }

    @PostMapping(path = "/register", produces = "application/json")
    public ResponseEntity<ResponseDTO> registerGroup(@RequestBody @Valid GroupDTO groupDTO, BindingResult bindingResult, HttpServletRequest request) throws MessagingException {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        UUID entityId = groupService.registerGroup(groupDTO, localeResolver.resolveLocale(request));
        return ResponseEntity.ok(createResponseDTO(entityId));
    }

    @PostMapping(path = "/addMembers", produces = "application/json")
    public ResponseEntity<ResponseDTO> addMembersToGroup(@RequestBody @Valid GroupInviteDTO groupInviteDTO, BindingResult bindingResult, HttpServletRequest request) throws MessagingException {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        groupService.addMembersToGroup(groupInviteDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PostMapping(path = "/addMember", produces = "application/json")
    public ResponseEntity<ResponseDTO> addSingleMemberToGroup(@RequestBody @Valid GroupMemberInviteDTO groupMemberInviteDTO, BindingResult bindingResult, HttpServletRequest request) throws MessagingException {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        groupService.addSingleMemberToGroup(groupMemberInviteDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PostMapping(path = "/addAdmin", produces = "application/json")
    public ResponseEntity<ResponseDTO> addSingleAdmin(@RequestBody @Valid GroupAdminDTO groupAdminDTO, BindingResult bindingResult, HttpServletRequest request) throws MessagingException {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        groupService.addSingleAdmin(groupAdminDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PostMapping(path = "/removeMember", produces = "application/json")
    public ResponseEntity<ResponseDTO> removeMember(@RequestBody @Valid GroupMemberRemoveDTO groupMemberRemoveDTO, BindingResult bindingResult, HttpServletRequest request) throws MessagingException {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        groupService.removeMember(groupMemberRemoveDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PutMapping(path = "/edit", produces = "application/json")
    public ResponseEntity<ResponseDTO> editGroup(@RequestBody @Valid GroupDTO groupDTO, BindingResult bindingResult, HttpServletRequest request) throws MessagingException {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        } else if (groupDTO.getId() == null) {
            return ResponseEntity.badRequest().build();
        }

        groupService.editGroup(groupDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PostMapping(path = "/removeAdmin", produces = "application/json")
    public ResponseEntity<ResponseDTO> removeAdmin(@RequestBody @Valid GroupAdminRemoveDTO groupAdminRemoveDTO, BindingResult bindingResult, HttpServletRequest request) throws MessagingException {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.unprocessableEntity().body(createResponseDTO(bindingResult, request));
        }

        groupService.removeAdmin(groupAdminRemoveDTO);
        return ResponseEntity.ok(createResponseDTO());
    }

    @PutMapping(path = "/invite", produces = "application/json")
    public ResponseEntity<ResponseDTO> handleInvite(@RequestParam Long inviteId, @RequestParam Boolean response) {
        if (inviteId == null || response == null) return ResponseEntity.badRequest().build();

        if (response) {
            groupService.acceptInvite(inviteId);
        } else {
            groupService.declineInvite(inviteId);
        }

        return ResponseEntity.ok(createResponseDTO());
    }

    @DeleteMapping(path = "/delete", produces = "application/json")
    public ResponseEntity<ResponseDTO> deleteGroup(@RequestParam UUID groupId) {
        if (groupId == null) return ResponseEntity.badRequest().build();

        groupService.deleteGroup(groupId);
        return ResponseEntity.ok(createResponseDTO());
    }


    @InitBinder("groupDTO")
    private void initGroupDTOBinder(WebDataBinder binder) {
        binder.addValidators(groupDTOValidator);
    }
}
