package hr.zeroinfinity.backend.controllers.mvc;

import hr.zeroinfinity.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Controller
public class MVCController {

    private static final String HOME_LINK_TEMPLATE = "home_%s";
    private static final String ACCOUNT_VERIFICATION_LINK_TEMPLATE = "account-verification_%s";

    private final LocaleResolver localeResolver;
    private final UserService userService;

    @Autowired
    public MVCController(LocaleResolver localeResolver, UserService userService) {
        this.localeResolver = localeResolver;
        this.userService = userService;
    }

    @RequestMapping(value = {"", "/", "/home"}, method = RequestMethod.GET)
    public String showHome(HttpServletRequest request) {
        return String.format(HOME_LINK_TEMPLATE, localeResolver.resolveLocale(request).toString());
    }

    @RequestMapping(value = "account/confirm", method = RequestMethod.GET)
    public String verifyAccount(HttpServletRequest request, @RequestParam(value = "key") UUID key) {
        userService.verifyUser(key);

        return String.format(ACCOUNT_VERIFICATION_LINK_TEMPLATE, localeResolver.resolveLocale(request).toString());
    }

    @RequestMapping(value = "privacyPolicy", method = RequestMethod.GET)
    public String getPrivacyPolicy(){
        return "privacy_policy";
    }
}