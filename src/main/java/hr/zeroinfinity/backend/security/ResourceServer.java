package hr.zeroinfinity.backend.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

@EnableResourceServer
@Configuration
public class ResourceServer extends ResourceServerConfigurerAdapter {

    private static final String[] PUBLIC_ROUTES = {"/swagger-ui.html",
            "/webjars/**",
            "/h2-console/**",
            "/swagger-resources/**",
            "/",
            "/csrf",
            "/v2/api-docs",
            "/css/**",
            "/js/**",
            "/img/**",
            "/**/favicon.ico",
            "/account/confirm",
            "/user/register",
            "/user/forgotPassword",
            "/info/*",
            "/privacyPolicy"
    };

    private final DefaultTokenServices tokenServices;

    @Autowired
    public ResourceServer(DefaultTokenServices tokenServices) {
        this.tokenServices = tokenServices;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().sameOrigin();

        http.
                authorizeRequests()
                .antMatchers(PUBLIC_ROUTES).permitAll()
                .and().authorizeRequests().anyRequest().authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer config) {
        config.tokenServices(tokenServices);
    }
}
