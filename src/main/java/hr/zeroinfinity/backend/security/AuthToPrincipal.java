package hr.zeroinfinity.backend.security;

import hr.zeroinfinity.backend.dto.jwt.UserPrincipal;
import hr.zeroinfinity.backend.dto.jwt.UserTokenData;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.oauth2.provider.token.DefaultUserAuthenticationConverter;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Map;

/**
 * This configuration defines how will Principal object be generated from token included in http request header.
 */
@Configuration
public class AuthToPrincipal extends DefaultUserAuthenticationConverter {

    private static final String N_A = "N/A";
    private static final String ERROR_MESSAGE = "Authorities must be either a String or a Collection";

    @Value("${app.security.jwt-data-key}")
    private String USER_DATA_KEY;

    public AuthToPrincipal() {
        super();
    }

    @Override
    public Authentication extractAuthentication(Map<String, ?> map) {
        if (map.containsKey(USER_DATA_KEY)) {
            final ObjectMapper mapper = new ObjectMapper();
            final UserTokenData userTokenData = mapper.convertValue(map.get(USER_DATA_KEY), UserTokenData.class);

            UserPrincipal principal = new UserPrincipal(userTokenData, userTokenData.getEmail());

            Collection<? extends GrantedAuthority> authorities = getAuthorities(map);
            return new UsernamePasswordAuthenticationToken(principal, N_A, authorities);
        }

        return super.extractAuthentication(map);
    }

    private Collection<? extends GrantedAuthority> getAuthorities(Map<String, ?> map) {
        Object authorities = map.get(AUTHORITIES);

        if (authorities instanceof String) {
            return AuthorityUtils.commaSeparatedStringToAuthorityList((String) authorities);
        }

        if (authorities instanceof Collection) {
            return AuthorityUtils
                    .commaSeparatedStringToAuthorityList(
                            StringUtils.collectionToCommaDelimitedString((Collection<?>) authorities));
        }

        throw new IllegalArgumentException(ERROR_MESSAGE);
    }


}
