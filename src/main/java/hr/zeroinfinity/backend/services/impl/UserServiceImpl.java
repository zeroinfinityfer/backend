package hr.zeroinfinity.backend.services.impl;

import hr.zeroinfinity.backend.dto.UserDTO;
import hr.zeroinfinity.backend.dto.mappers.impl.UserMapper;
import hr.zeroinfinity.backend.entities.Device;
import hr.zeroinfinity.backend.entities.Group;
import hr.zeroinfinity.backend.entities.Registration;
import hr.zeroinfinity.backend.entities.Saving;
import hr.zeroinfinity.backend.entities.User;
import hr.zeroinfinity.backend.repositories.DeviceRepository;
import hr.zeroinfinity.backend.repositories.GroupRepository;
import hr.zeroinfinity.backend.repositories.RegistrationRepository;
import hr.zeroinfinity.backend.repositories.UserRepository;
import hr.zeroinfinity.backend.services.UserService;
import hr.zeroinfinity.backend.services.util.mail.CustomMailSender;
import hr.zeroinfinity.backend.services.util.mail.model.AccountConfirmationEmail;
import hr.zeroinfinity.backend.services.util.mail.model.ForgotPasswordEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

@Service
@Primary
public class UserServiceImpl extends AuthorizedService implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private static final String APPLICATION_LINK_KEY = "#APPLICATION_LINK#";
    private static final String USER_NAME_KEY = "#USER_NAME#";
    private static final String USER_SURNAME_KEY = "#USER_SURNAME#";
    private static final String USER_ID_KEY = "#USER_ID#";
    private static final String NEW_PASSWORD_KEY = "#NEW_PASSWORD_KEY#";
    private static final String ACCOUNT_CONFIRMATION_SUBJECT = "Account confirmation";
    private static final String FORGOT_PASSWORD_SUBJECT = "Forgot password";
    private static final String REGISTERED_SUCCESSFULLY_MESSAGE = "User %s registered successfully.";
    private static final String USER_VERIFIED_MESSAGE = "User with id %s verified.";
    private static final String DELETING_UNVERIFIED_ACCOUNTS_MESSAGE = "Deleting %d unverified accounts.";
    private static final String PERSONAL_GROUP_NAME_FORMAT = "%s %s";
    private static final String SIMPLE_USER = "simple user";


    @Value("${spring.mail.username}")
    private String APPLICATION_EMAIL;

    @Value("${app.baseurl}")
    private String APPLICATION_LINK;

    private final RegistrationRepository registrationRepository;
    private final UserMapper userMapper;
    private final CustomMailSender customMailSender;
    private final AccountConfirmationEmail accountConfirmationEmail;
    private final DeviceRepository deviceRepository;
    private final GroupRepository groupRepository;
    private final PasswordEncoder passwordEncoder;
    private final ForgotPasswordEmail forgotPasswordEmail;

    @Autowired
    public UserServiceImpl(
            UserRepository userRepository,
            RegistrationRepository registrationRepository,
            UserMapper userMapper,
            CustomMailSender customMailSender,
            AccountConfirmationEmail accountConfirmationEmail,
            DeviceRepository deviceRepository,
            GroupRepository groupRepository,
            @Lazy PasswordEncoder passwordEncoder,
            ForgotPasswordEmail forgotPasswordEmail) {

        super(userRepository);

        this.registrationRepository = registrationRepository;
        this.userMapper = userMapper;
        this.customMailSender = customMailSender;
        this.forgotPasswordEmail = forgotPasswordEmail;
        this.accountConfirmationEmail = accountConfirmationEmail;
        this.deviceRepository = deviceRepository;
        this.groupRepository = groupRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDTO getUserById(@NotNull UUID id) {
        return userMapper.entityToDto(userRepository.findById(id).orElseThrow(ENTITY_NOT_FOUND));
    }

    @Override
    public UserDTO getUserByEmail(@NotNull String email) {
        return userMapper.entityToDto(userRepository.findByEmail(email).orElseThrow(ENTITY_NOT_FOUND));
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email).orElseThrow(USERNAME_NOT_FOUND);

        Set<GrantedAuthority> auth = new HashSet<>();
        auth.add(new SimpleGrantedAuthority(SIMPLE_USER));

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPasswordHash(), auth);
    }

    @Override
    @Transactional(rollbackOn = {MessagingException.class, MailException.class})
    public void registerUser(@NotNull UserDTO dto, @NotNull Locale locale) throws MessagingException {
        User user = userMapper.dtoToEntity(dto);
        user = userRepository.save(user);
        saveRegistration(user.getId());
        sendAccountConfirmation(user);

        LOGGER.info(String.format(REGISTERED_SUCCESSFULLY_MESSAGE, dto.getEmail()));

        Group group = new Group();
        group.setAdmins(Arrays.asList(user));
        group.setMembers(Arrays.asList(user));
        group.setName(String.format(PERSONAL_GROUP_NAME_FORMAT, user.getName(), user.getSurname()));
        group.setCurrency(dto.getCurrency());
        group.setBalance(BigDecimal.ZERO);
        group.setPersonal(dto.getPersonalGroupEnabled() == null ? true : dto.getPersonalGroupEnabled());

        Saving saving = new Saving();
        saving.setAmount(BigDecimal.ZERO);
        saving.setGroup(group);
        group.setSaving(saving);

        groupRepository.save(group);
    }

    @Override
    public void verifyUser(UUID userId) {
        Registration registration = registrationRepository.findByUserId(userId).orElseThrow(ENTITY_NOT_FOUND);
        User user = userRepository.findById(registration.getUserId()).orElseThrow(ENTITY_NOT_FOUND);
        registrationRepository.delete(registration);

        LOGGER.info(String.format(USER_VERIFIED_MESSAGE, userId.toString()));

        user.setVerified(true);
        userRepository.save(user);
    }

    @Override
    public void registerDevice(String deviceCode) {
        User user = getActiveUserInternal();
        List<Device> devices = user.getDevices();
        if(devices.stream().anyMatch(d -> d.getDeviceCode().equals(deviceCode))) return;

        Device device = new Device();
        device.setDeviceCode(deviceCode);
        device.setUser(user);

        deviceRepository.save(device);
    }

    @Override
    public void removeDevice(String deviceCode) {
        Device device = deviceRepository.findFirstByDeviceCode(deviceCode);
        if (device != null) {
            deviceRepository.delete(device);
        }
    }


    @Override
    @Transactional(rollbackOn = {MessagingException.class, MailException.class})
    public void resetPassword(@NotNull String email) throws MessagingException {
        User user = userRepository.findByEmail(email).orElseThrow(ENTITY_NOT_FOUND);

        String password = UUID.randomUUID().toString();
        user.setPasswordHash(passwordEncoder.encode(password));
        userRepository.save(user);

        sendForgotEmail(user, password);
    }

    @Override
    public void changePersonalGroupState(boolean enabled) {
        User active = getActiveUserInternal();
        active.setPersonalGroupEnabled(enabled);
        userRepository.save(active);
    }

    /**
     * Used for cleaning up unverified accounts every day; account must be verified in 7 days period.
     */
    @Scheduled(initialDelay = 1000 * 60 * 60 * 24, fixedDelay = 1000 * 60 * 60 * 24)
    private void deleteExpiredRegistrations() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        Date weekFromNow = cal.getTime();

        List<Registration> registrations = registrationRepository.findAllByCreatedAtBefore(weekFromNow);
        LOGGER.info(DELETING_UNVERIFIED_ACCOUNTS_MESSAGE, registrations.size());
        registrations.forEach(registration -> userRepository.deleteById(registration.getUserId()));

    }

    /**
     * Sends account confirmation email which contains the account verification link.
     *
     * @param user user entity
     * @throws MessagingException if messaging exception occurs
     */
    private void sendAccountConfirmation(User user) throws MessagingException {
        accountConfirmationEmail.setArgument(APPLICATION_LINK_KEY, APPLICATION_LINK);
        accountConfirmationEmail.setArgument(USER_ID_KEY, user.getId().toString());
        accountConfirmationEmail.setArgument(USER_NAME_KEY, user.getName());
        accountConfirmationEmail.setArgument(USER_SURNAME_KEY, user.getSurname());

        customMailSender
                .create()
                .setFrom(APPLICATION_EMAIL)
                .setTo(user.getEmail())
                .setSubject(ACCOUNT_CONFIRMATION_SUBJECT)
                .send(accountConfirmationEmail, true);
    }

    private void sendForgotEmail(User user, String password) throws MessagingException {
        forgotPasswordEmail.setArgument(NEW_PASSWORD_KEY, password);

        customMailSender
                .create()
                .setFrom(APPLICATION_EMAIL)
                .setTo(user.getEmail())
                .setSubject(FORGOT_PASSWORD_SUBJECT)
                .send(forgotPasswordEmail, true);
    }

    /**
     * Saves registration of some user which will later be verified or deleted.
     *
     * @param userId user unique id
     */
    private void saveRegistration(UUID userId) {
        Registration registration = new Registration();
        registration.setUserId(userId);
        registration.setCreatedAt(new Date());

        registrationRepository.save(registration);
    }
}
