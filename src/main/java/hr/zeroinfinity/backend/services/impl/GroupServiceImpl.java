package hr.zeroinfinity.backend.services.impl;

import hr.zeroinfinity.backend.dto.GroupAdminDTO;
import hr.zeroinfinity.backend.dto.GroupAdminRemoveDTO;
import hr.zeroinfinity.backend.dto.GroupDTO;
import hr.zeroinfinity.backend.dto.GroupInviteDTO;
import hr.zeroinfinity.backend.dto.GroupMemberInviteDTO;
import hr.zeroinfinity.backend.dto.GroupMemberRemoveDTO;
import hr.zeroinfinity.backend.dto.InviteDTO;
import hr.zeroinfinity.backend.dto.mappers.impl.GroupMapper;
import hr.zeroinfinity.backend.dto.mappers.impl.InviteMapper;
import hr.zeroinfinity.backend.dto.updaters.impl.GroupUpdater;
import hr.zeroinfinity.backend.entities.Group;
import hr.zeroinfinity.backend.entities.Invite;
import hr.zeroinfinity.backend.entities.Saving;
import hr.zeroinfinity.backend.entities.User;
import hr.zeroinfinity.backend.repositories.GroupRepository;
import hr.zeroinfinity.backend.repositories.InviteRepository;
import hr.zeroinfinity.backend.repositories.UserRepository;
import hr.zeroinfinity.backend.services.FinanceService;
import hr.zeroinfinity.backend.services.GroupService;
import hr.zeroinfinity.backend.services.util.mail.CustomMailSender;
import hr.zeroinfinity.backend.services.util.mail.model.InviteSenderEmail;
import hr.zeroinfinity.backend.services.util.notification.CustomNotificationSender;
import hr.zeroinfinity.backend.services.util.notification.model.InviteNotificationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl extends AuthorizedService implements GroupService {

    private static final String ADMIN_EMAIL = "#ADMIN_EMAIL#";
    private static final String LINK = "#APPLICATION_LINK#";
    private static final String INVITE_SENDER_SUBJECT = "Invite send";
    private static final String ERROR_MESSAGE = "User not allowed to perform such activity.";
    private static final UnauthorizedUserException UNAUTHORIZED_USER_EXCEPTION = new UnauthorizedUserException(ERROR_MESSAGE);
    private static final Comparator<GroupDTO> GROUP_DTO_COMPARATOR = Comparator.comparing(GroupDTO::getPersonal).reversed();

    @Value("${spring.mail.username}")
    private String APPLICATION_EMAIL;
    @Value("${app.baseurl}")
    private String APPLICATION_LINK;


    private final GroupRepository groupRepository;
    private final CustomMailSender customMailSender;
    private final CustomNotificationSender customNotificationSender;
    private final InviteMapper inviteMapper;
    private final GroupMapper groupMapper;
    private final InviteRepository inviteRepository;
    private final InviteSenderEmail inviteSenderEmail;
    private final GroupUpdater groupUpdater;
    private final FinanceService financeService;

    @Autowired
    public GroupServiceImpl(
            GroupRepository groupRepository,
            UserRepository userRepository,
            CustomMailSender customMailSender,
            CustomNotificationSender customNotificationSender,
            GroupMapper groupMapper,
            InviteRepository inviteRepository,
            InviteSenderEmail inviteSenderEmail,
            GroupUpdater groupUpdater,
            @Lazy FinanceService financeService,
            InviteMapper inviteMapper) {

        super(userRepository);

        this.groupRepository = groupRepository;
        this.userRepository = userRepository;
        this.customMailSender = customMailSender;
        this.customNotificationSender = customNotificationSender;
        this.groupMapper = groupMapper;
        this.inviteRepository = inviteRepository;
        this.inviteSenderEmail = inviteSenderEmail;
        this.groupUpdater = groupUpdater;
        this.financeService = financeService;
        this.inviteMapper = inviteMapper;
    }

    @Transactional
    @Override
    public UUID registerGroup(GroupDTO groupDTO, Locale locale) throws MessagingException {
        Set<User> registeredUsers = new HashSet<>();
        Set<String> unregisteredUsers = new HashSet<>();
        Set<String> members = new HashSet<>(groupDTO.getInvitationEmails());
        User admin = getActiveUserInternal();

        sortInvitations(members, registeredUsers, unregisteredUsers);
        Group group = groupMapper.dtoToEntity(groupDTO);
        group.setCurrency(groupDTO.getCurrency());
        group.setBalance(BigDecimal.ZERO);
        group.setMembers(Arrays.asList(admin));
        group.setAdmins(Arrays.asList(admin));
        admin.getGroups().add(group);

        Saving saving = new Saving();
        saving.setAmount(BigDecimal.ZERO);
        saving.setGroup(group);
        group.setSaving(saving);

        group = groupRepository.save(group);

        saveInvites(members, group.getId(), getActiveUserInternal());
        sendEmailInvites(unregisteredUsers, admin.getEmail());
        sendNotificationInvites(registeredUsers, admin.getEmail(), group.getName());

        return group.getId();
    }

    @Override
    public void editGroup(GroupDTO groupDTO) {
        Group group = groupRepository.findById(groupDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        if (group.getPersonal()) {
            throw UNAUTHORIZED_USER_EXCEPTION;
        }

        groupUpdater.updateEntity(group, groupDTO);
        groupRepository.save(group);
    }

    @Override
    @Transactional
    public List<GroupDTO> getGroupsOfCurrentUser(Locale locale) {
        User active = getActiveUserInternal();
        boolean personalGroupEnabled = active.getPersonalGroupEnabled();

        return active
                .getGroups()
                .stream()
                .filter(group -> {
                    if (personalGroupEnabled) {
                        return true;
                    } else {
                        if (group.getPersonal()) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                })
                .map(group -> {
                    GroupDTO dto = groupMapper.entityToDto(group);
                    dto.setCategories(financeService.getCategoriesByGroupId(group.getId(), locale));

                    return dto;
                })
                .sorted(GROUP_DTO_COMPARATOR)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public boolean acceptInvite(Long inviteId) {
        Invite invite = inviteRepository.findById(inviteId).orElseThrow(ENTITY_NOT_FOUND);
        User activeUser = getActiveUserInternal();
        Optional<Group> opt = groupRepository.findById(invite.getGroupId());

        if (!opt.isPresent()) {
            inviteRepository.delete(invite);
            return false;
        }

        Group group = opt.get();
        if (!activeUser.getEmail().equals(invite.getEmail())) {
            throw UNAUTHORIZED_USER_EXCEPTION;
        }

        group.getMembers().add(activeUser);
        inviteRepository.delete(invite);
        return true;
    }

    @Override
    @Transactional
    public void declineInvite(Long inviteId) {
        Invite invite = inviteRepository.findById(inviteId).orElseThrow(ENTITY_NOT_FOUND);
        User activeUser = getActiveUserInternal();

        if (!activeUser.getEmail().equals(invite.getEmail())) {
            throw UNAUTHORIZED_USER_EXCEPTION;
        }

        inviteRepository.delete(invite);
    }

    @Override
    public void addMembersToGroup(GroupInviteDTO groupInviteDTO) throws MessagingException {
        Set<User> registeredUsers = new HashSet<>();
        Set<String> unregisteredUsers = new HashSet<>();
        Set<String> members = new HashSet<>(groupInviteDTO.getMembers());

        Group group = groupRepository.findById(groupInviteDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        if (group.getPersonal()) {
            throw UNAUTHORIZED_USER_EXCEPTION;
        }

        sortInvitations(members, registeredUsers, unregisteredUsers);
        saveInvites(members, groupInviteDTO.getId(), getActiveUserInternal());

        sendEmailInvites(unregisteredUsers, getActiveUserInternal().getEmail());
        sendNotificationInvites(registeredUsers, getActiveUserInternal().getEmail(), group.getName());
    }

    @Override
    public void addSingleMemberToGroup(GroupMemberInviteDTO groupMemberInviteDTO) throws MessagingException {
        String email = groupMemberInviteDTO.getMemberEmail();
        Group group = groupRepository.findById(groupMemberInviteDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        if (group.getPersonal()) {
            throw UNAUTHORIZED_USER_EXCEPTION;
        }

        saveInvites(new HashSet<>(Collections.singletonList(email)), groupMemberInviteDTO.getId(), getActiveUserInternal());

        Optional<User> user = userRepository.findByEmail(email);
        if (!user.isPresent()) {
            sendEmailInvites(new HashSet<>(Collections.singletonList(email)), getActiveUserInternal().getEmail());
        } else {
            InviteNotificationModel model = new InviteNotificationModel(getActiveUserInternal().getEmail(), group.getName());
            customNotificationSender.sendNotification(model, user.get());
        }
    }

    @Override
    public void addSingleAdmin(GroupAdminDTO groupAdminDTO) {
        UUID userId = groupAdminDTO.getUserId();
        UUID groupId = groupAdminDTO.getGroupId();

        User user = userRepository.findById(userId).orElseThrow(ENTITY_NOT_FOUND);
        Group group = groupRepository.findByIdAndFetchAttachedAdmins(groupId);
        if (group.getPersonal()) {
            throw UNAUTHORIZED_USER_EXCEPTION;
        }

        group.getAdmins().add(user);
        groupRepository.save(group);
    }

    @Override
    public void removeMember(GroupMemberRemoveDTO groupMemberRemoveDTO) {
        UUID userId = groupMemberRemoveDTO.getUserId();
        UUID groupId = groupMemberRemoveDTO.getGroupId();

        User user = userRepository.findById(userId).orElseThrow(ENTITY_NOT_FOUND);
        User active = getActiveUserInternal();
        Group group = groupRepository.findByIdAndFetchAttachedAdmins(groupId);
        if (group.getPersonal()) {
            throw UNAUTHORIZED_USER_EXCEPTION;
        }

        if (!(group.getAdmins().contains(active) || user.equals(active))) {
            throw UNAUTHORIZED_USER_EXCEPTION;
        }

        group.getAdmins().remove(user);
        groupRepository.save(group);
        group = groupRepository.findByIdAndFetchAttachedMembers(groupId);
        group.getMembers().remove(user);
        groupRepository.save(group);

        if (groupRepository.countGroupAdmins(groupId) == 0) {
            groupRepository.delete(group);
        }
    }

    @Override
    public void deleteGroup(UUID groupId) {
        checkIfIsAdmin(groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND));
        groupRepository.deleteById(groupId);
    }

    @Override
    public void removeAdmin(GroupAdminRemoveDTO groupAdminRemoveDTO) {
        UUID groupId = groupAdminRemoveDTO.getGroupId();
        User user = getActiveUserInternal();

        Group group = groupRepository.findByIdAndFetchAttachedAdmins(groupId);
        group.getAdmins().remove(user);
        groupRepository.save(group);
    }

    @Override
    public List<InviteDTO> getInvitesOfCurrentUser() {
        return inviteRepository
                .findByEmail(SecurityContextHolder.getContext().getAuthentication().getName())
                .stream()
                .map(inviteMapper::entityToDto)
                .collect(Collectors.toList());
    }

    private void sortInvitations(Set<String> members, Set<User> registeredUsers, Set<String> unregisteredUsers) {
        for (String email : members) {
            Optional<User> user = userRepository.findByEmail(email);
            if (user.isPresent()) {
                registeredUsers.add(user.get());
            } else {
                unregisteredUsers.add(email);
            }
        }
    }

    private void sendEmailInvites(Set<String> unregisteredUsers, String adminEmail) throws MessagingException {
        for (String email : unregisteredUsers) {
            inviteSenderEmail.setArgument(ADMIN_EMAIL, adminEmail);
            inviteSenderEmail.setArgument(LINK, APPLICATION_LINK);

            customMailSender
                    .create()
                    .setFrom(APPLICATION_EMAIL)
                    .setTo(email)
                    .setSubject(INVITE_SENDER_SUBJECT)
                    .send(inviteSenderEmail, true);
        }
    }

    private void sendNotificationInvites(Set<User> registeredUsers, String adminEmail, String groupName) {
        InviteNotificationModel model = new InviteNotificationModel(adminEmail, groupName);
        for(User user: registeredUsers){
            customNotificationSender.sendNotification(model, user);
        }
    }

    private void saveInvites(Set<String> emails, UUID groupId, User invitedBy) {
        for (String email : emails) {
            if (inviteRepository.existsByEmailAndGroupId(email, groupId)) continue;

            Invite invite = new Invite();
            invite.setEmail(email);
            invite.setGroupId(groupId);
            invite.setCreatedAt(new Date());
            invite.setInvitedBy(invitedBy);

            inviteRepository.save(invite);
        }
    }
}
