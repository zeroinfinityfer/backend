package hr.zeroinfinity.backend.services.impl;

import hr.zeroinfinity.backend.entities.Group;
import hr.zeroinfinity.backend.entities.User;
import hr.zeroinfinity.backend.repositories.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;

import javax.persistence.EntityNotFoundException;
import java.util.function.Supplier;

public abstract class AuthorizedService {

    protected static final Supplier<RuntimeException> USERNAME_NOT_FOUND = () -> new UsernameNotFoundException("User with provided email has not been registered or hasn't verified his account.");
    protected static final Supplier<RuntimeException> ENTITY_NOT_FOUND = () -> new EntityNotFoundException("Entity not found!");

    protected UserRepository userRepository;

    protected AuthorizedService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    protected User getActiveUserInternal() {
        return userRepository
                .findByEmail(SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getName()).orElseThrow(USERNAME_NOT_FOUND);
    }

    protected void checkIfIsMember(Group group) {
        User activeUser = getActiveUserInternal();
        if (!group.getMembers().contains(activeUser)) {
            throw new UnauthorizedUserException(
                    String.format("User %s isn't member of group %s", activeUser.getEmail(), group.getId()));
        }
    }

    protected void checkIfIsAdmin(Group group) {
        User activeUser = getActiveUserInternal();
        if (!group.getAdmins().contains(activeUser)) {
            throw new UnauthorizedUserException(
                    String.format("User %s isn't admin of group %s", activeUser.getEmail(), group.getId()));
        }
    }
}
