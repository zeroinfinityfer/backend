package hr.zeroinfinity.backend.services.impl;

import hr.zeroinfinity.backend.dto.CalendarStatisticsDTO;
import hr.zeroinfinity.backend.dto.CategoryDTO;
import hr.zeroinfinity.backend.dto.DailyStatisticsDTO;
import hr.zeroinfinity.backend.dto.ExpenseDTO;
import hr.zeroinfinity.backend.dto.FinanceStatisticsDTO;
import hr.zeroinfinity.backend.dto.GoalDTO;
import hr.zeroinfinity.backend.dto.IncomeDTO;
import hr.zeroinfinity.backend.dto.PairDTO;
import hr.zeroinfinity.backend.dto.SavingEntryDTO;
import hr.zeroinfinity.backend.dto.SavingStatisticsDTO;
import hr.zeroinfinity.backend.dto.mappers.impl.CategoryMapper;
import hr.zeroinfinity.backend.dto.mappers.impl.ExpenseMapper;
import hr.zeroinfinity.backend.dto.mappers.impl.GoalMapper;
import hr.zeroinfinity.backend.dto.mappers.impl.IncomeMapper;
import hr.zeroinfinity.backend.dto.mappers.impl.SavingEntryMapper;
import hr.zeroinfinity.backend.dto.updaters.impl.CategoryUpdater;
import hr.zeroinfinity.backend.dto.updaters.impl.ExpenseUpdater;
import hr.zeroinfinity.backend.dto.updaters.impl.GoalUpdater;
import hr.zeroinfinity.backend.dto.updaters.impl.IncomeUpdater;
import hr.zeroinfinity.backend.entities.Category;
import hr.zeroinfinity.backend.entities.Expense;
import hr.zeroinfinity.backend.entities.Goal;
import hr.zeroinfinity.backend.entities.Group;
import hr.zeroinfinity.backend.entities.Income;
import hr.zeroinfinity.backend.entities.Saving;
import hr.zeroinfinity.backend.entities.SavingEntry;
import hr.zeroinfinity.backend.repositories.CategoryRepository;
import hr.zeroinfinity.backend.repositories.ExpenseRepository;
import hr.zeroinfinity.backend.repositories.GoalRepository;
import hr.zeroinfinity.backend.repositories.GroupRepository;
import hr.zeroinfinity.backend.repositories.IncomeRepository;
import hr.zeroinfinity.backend.repositories.SavingEntryRepository;
import hr.zeroinfinity.backend.repositories.SavingRepository;
import hr.zeroinfinity.backend.repositories.UserRepository;
import hr.zeroinfinity.backend.services.FinanceService;
import hr.zeroinfinity.backend.services.util.FinanceServiceCollectors;
import hr.zeroinfinity.backend.services.util.mail.CustomMailSender;
import hr.zeroinfinity.backend.services.util.notification.CustomNotificationSender;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static hr.zeroinfinity.backend.services.util.TimeUtil.resolveDate;
import static hr.zeroinfinity.backend.services.util.TimeUtil.resolveDatedPairs;

@Service
public class FinanceServiceImpl extends AuthorizedService implements FinanceService {

    private static final String GOAL_ACCEPTED = "goal.accepted";
    private static final Function<Map.Entry<String, Double>, String> KEY_EXTRACTOR = Map.Entry::getKey;
    private static final Comparator<Map.Entry<String, Double>> COMPARATOR_BY_KEY = Comparator.comparing(KEY_EXTRACTOR);
    private static final Function<Map.Entry<String, Double>, PairDTO> ENTRY_PAIR_DTO_FUNCTION = e -> new PairDTO(e.getKey(), e.getValue());
    private static final Function<CategoryDTO, @NotEmpty(message = "form.basic.notempty") @Size(max = 128, message = "form.basic.inputTooLong") String> GET_NAME = CategoryDTO::getName;

    private final CategoryRepository categoryRepository;
    private final ExpenseRepository expenseRepository;
    private final GoalRepository goalRepository;
    private final GroupRepository groupRepository;
    private final IncomeRepository incomeRepository;
    private final SavingRepository savingRepository;
    private final CustomMailSender customMailSender;
    private final CustomNotificationSender customNotificationSender;
    private final ExpenseMapper expenseMapper;
    private final GoalMapper goalMapper;
    private final IncomeMapper incomeMapper;
    private final CategoryMapper categoryMapper;
    private final ExpenseUpdater expenseUpdater;
    private final GoalUpdater goalUpdater;
    private final IncomeUpdater incomeUpdater;
    private final CategoryUpdater categoryUpdater;
    private final MessageSource messageSource;
    private List<CategoryDTO> defaultCategories;
    private CategoryDTO goalCategory;
    private final SavingEntryMapper savingEntryMapper;
    private final SavingEntryRepository savingEntryRepository;

    public FinanceServiceImpl(
            CategoryRepository categoryRepository,
            ExpenseRepository expenseRepository,
            GoalRepository goalRepository,
            IncomeRepository incomeRepository,
            SavingRepository savingRepository,
            GroupRepository groupRepository,
            UserRepository userRepository,
            CustomMailSender customMailSender,
            CustomNotificationSender customNotificationSender,
            ExpenseMapper expenseMapper,
            GoalMapper goalMapper,
            IncomeMapper incomeMapper,
            CategoryMapper categoryMapper,
            ExpenseUpdater expenseUpdater,
            GoalUpdater goalUpdater,
            IncomeUpdater incomeUpdater,
            CategoryUpdater categoryUpdater,
            SavingEntryMapper savingEntryMapper,
            SavingEntryRepository savingEntryRepository,
            @Lazy MessageSource messageSource) {

        super(userRepository);

        this.categoryRepository = categoryRepository;
        this.expenseRepository = expenseRepository;
        this.goalRepository = goalRepository;
        this.incomeRepository = incomeRepository;
        this.savingRepository = savingRepository;
        this.groupRepository = groupRepository;
        this.customMailSender = customMailSender;
        this.customNotificationSender = customNotificationSender;
        this.expenseMapper = expenseMapper;
        this.goalMapper = goalMapper;
        this.incomeMapper = incomeMapper;
        this.categoryMapper = categoryMapper;
        this.expenseUpdater = expenseUpdater;
        this.goalUpdater = goalUpdater;
        this.incomeUpdater = incomeUpdater;
        this.categoryUpdater = categoryUpdater;
        this.savingEntryMapper = savingEntryMapper;
        this.savingEntryRepository = savingEntryRepository;
        this.messageSource = messageSource;
    }

    @PostConstruct
    private void initDefaultValues() {
        this.defaultCategories = categoryRepository
                .findDefault()
                .stream()
                .map(categoryMapper::entityToDto)
                .collect(Collectors.toList());

        this.goalCategory = this.defaultCategories
                .stream()
                .filter(c -> c.getName().equals("categories.fulfilledGoals"))
                .findFirst()
                .orElseThrow(RuntimeException::new);
    }

    @Transactional
    @Override
    public Long registerCategory(CategoryDTO categoryDTO) {
        checkIfIsMember(groupRepository.findById(categoryDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND));

        Category category = categoryMapper.dtoToEntity(categoryDTO);
        category = categoryRepository.save(category);

        return category.getId();
    }

    @Transactional
    @Override
    public Long registerExpense(ExpenseDTO expenseDTO) {
        Group group = groupRepository.findById(expenseDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(group);

        Expense expense = expenseMapper.dtoToEntity(expenseDTO);
        expense = expenseRepository.save(expense);

        group.setBalance(group.getBalance().subtract(expenseDTO.getAmount()));
        groupRepository.save(group);

        return expense.getId();
    }

    @Transactional
    @Override
    public Long registerGoal(GoalDTO goalDTO) {
        checkIfIsMember(groupRepository.findById(goalDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND));

        Goal goal = goalMapper.dtoToEntity(goalDTO);
        goal = goalRepository.save(goal);

        return goal.getId();
    }

    @Transactional
    @Override
    public Long registerIncome(IncomeDTO incomeDTO) {
        Group group = groupRepository.findById(incomeDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(group);

        Income income = incomeMapper.dtoToEntity(incomeDTO);
        income = incomeRepository.save(income);

        group.setBalance(group.getBalance().add(incomeDTO.getAmount()));
        groupRepository.save(group);

        return income.getId();
    }

    @Transactional
    @Override
    public void deleteExpense(Long expenseId) {
        Expense expense = expenseRepository.findById(expenseId).orElseThrow(ENTITY_NOT_FOUND);
        Group group = expense.getGroup();
        checkIfIsMember(group);

        group.setBalance(group.getBalance().add(expense.getAmount()));

        groupRepository.save(group);
        expenseRepository.delete(expense);

        //todo notify group members
    }

    @Transactional
    @Override
    public void deleteCategory(Long categoryId) {
        Category category = categoryRepository.findById(categoryId).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(category.getGroup());

        categoryRepository.delete(category);

        //todo notify group members
    }

    @Transactional
    @Override
    public void deleteGoal(Long goalId) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(goal.getSaving().getGroup());
        goalRepository.delete(goal);

        //todo notify group members
    }

    @Transactional
    @Override
    public void deleteIncome(Long incomeId) {
        Income income = incomeRepository.findById(incomeId).orElseThrow(ENTITY_NOT_FOUND);
        Group group = income.getGroup();
        checkIfIsMember(group);

        group.setBalance(group.getBalance().subtract(income.getAmount()));

        groupRepository.save(group);
        incomeRepository.delete(income);

        //todo notify group members
    }

    @Transactional(value = Transactional.TxType.REQUIRED)
    @Override
    public List<CategoryDTO> getCategoriesByGroupId(UUID groupId, Locale locale) {
        checkIfIsMember(groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND));

        List<CategoryDTO> result = categoryRepository
                .findAllByGroup(groupId)
                .stream()
                .map(categoryMapper::entityToDto)
                .collect(Collectors.toList());

        result.addAll(defaultCategories.stream()
                .map(c -> new CategoryDTO(
                        c.getId(),
                        messageSource.getMessage(c.getName(), null, locale),
                        c.getGroupId())
                )
                .collect(Collectors.toList()));

        return result;
    }

    @Transactional
    @Override
    public Page<ExpenseDTO> getExpensesByGroupIdPageable(UUID groupId, Pageable pageable) {
        checkIfIsMember(groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND));
        Page<Expense> expenses = expenseRepository.findAllByGroupId(groupId, pageable);

        return new PageImpl<>(
                expenses.stream()
                        .map(expenseMapper::entityToDto)
                        .collect(Collectors.toList()), pageable, expenses.getTotalElements());
    }

    @Transactional
    @Override
    public Page<GoalDTO> getGoalsByGroupIdPageable(UUID groupId, Pageable pageable) {
        Group group = groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(group);

        Long savingId = group.getSaving().getId();
        Page<Goal> goals = goalRepository.findAllBySavingId(savingId, pageable);

        return new PageImpl<>(goals
                .stream()
                .map(goalMapper::entityToDto)
                .collect(Collectors.toList()), pageable, goals.getTotalElements());
    }

    @Transactional
    @Override
    public Page<IncomeDTO> getIncomesByGroupIdPageable(UUID groupId, Pageable pageable) {
        checkIfIsMember(groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND));
        Page<Income> incomes = incomeRepository.findAllByGroupId(groupId, pageable);

        return new PageImpl<>(
                incomes.stream()
                        .map(incomeMapper::entityToDto)
                        .collect(Collectors.toList()), pageable, incomes.getTotalElements());
    }

    @Transactional
    @Override
    public void editExpense(ExpenseDTO expenseDTO) {
        Group group = groupRepository.findById(expenseDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(group);

        Expense expense = expenseRepository.findById(expenseDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        BigDecimal oldAmount = expense.getAmount();
        expenseUpdater.updateEntity(expense, expenseDTO);

        BigDecimal balance = group.getBalance();
        balance = balance.add(oldAmount);
        group.setBalance(balance.subtract(expense.getAmount()));

        groupRepository.save(group);
        expenseRepository.save(expense);
        //todo notify group members

    }

    @Transactional
    @Override
    public void editGoal(GoalDTO goalDTO) {
        checkIfIsMember(groupRepository.findById(goalDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND));

        Goal goal = goalRepository.findById(goalDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        goalUpdater.updateEntity(goal, goalDTO);

        goalRepository.save(goal);
        //todo notify group members
    }

    @Transactional
    @Override
    public void editIncome(IncomeDTO incomeDTO) {
        Group group = groupRepository.findById(incomeDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(group);

        Income income = incomeRepository.findById(incomeDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        BigDecimal oldAmount = income.getAmount();
        incomeUpdater.updateEntity(income, incomeDTO);

        BigDecimal balance = group.getBalance();
        balance = balance.subtract(oldAmount);
        group.setBalance(balance.add(incomeDTO.getAmount()));

        groupRepository.save(group);
        incomeRepository.save(income);
        //todo notify group members
    }

    @Override
    @Transactional
    public void editCategory(CategoryDTO categoryDTO) {
        checkIfIsMember(groupRepository.findById(categoryDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND));

        Category category = categoryRepository.findById(categoryDTO.getId()).orElseThrow(ENTITY_NOT_FOUND);
        categoryUpdater.updateEntity(category, categoryDTO);

        categoryRepository.save(category);
        //todo notify group members
    }

    @Override
    @Transactional
    public Boolean approveGoal(Long goalId, Locale locale, Date today) {
        Goal goal = goalRepository.findById(goalId).orElseThrow(ENTITY_NOT_FOUND);
        Group group = goal.getSaving().getGroup();
        checkIfIsMember(group);

        Saving saving = group.getSaving();
        BigDecimal amount = saving.getAmount();
        if (amount.compareTo(goal.getTargetAmount()) < 0) {
            return false;
        }

        BigDecimal newAmount = amount.subtract(goal.getTargetAmount());
        saving.setAmount(newAmount);

        Expense expense = new Expense();
        expense.setAmount(goal.getTargetAmount());
        expense.setDateCreated(new Date());
        expense.setGroup(group);
        expense.setCategory(categoryRepository.findById(goalCategory.getId()).orElseThrow(ENTITY_NOT_FOUND));
        expense.setDescription(String.format(messageSource.getMessage(GOAL_ACCEPTED, null, locale), goal.getName()));

        SavingEntry savingEntry = new SavingEntry();
        savingEntry.setSaving(saving);
        savingEntry.setCurrentAmount(newAmount);
        savingEntry.setAmountAdded(goal.getTargetAmount().negate());
        savingEntry.setDateCreated(today);

        group.setBalance(group.getBalance().subtract(goal.getTargetAmount()));
        expenseRepository.save(expense);
        groupRepository.save(group);
        goalRepository.delete(goal);
        savingEntryRepository.save(savingEntry);

        return true;

    }


    @Override
    @Transactional
    public Map<String, Double> getExpensesStatistics(@NotNull UUID groupId, @NotNull String periodUnit, @NotNull Integer periodLength, Locale locale, @NotNull Date toDate) {
        checkIfIsMember(groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND));
        Date fromDate = resolveDate(periodUnit, periodLength, toDate);
        List<Expense> expenses = expenseRepository.findAllInRange(groupId, fromDate, toDate);

        if (expenses.size() == 0) {
            return Collections.emptyMap();
        }

        Collector<Expense, Map<String, Double>, Map<String, Double>> EXPENSE_COLLECTOR_BY_CATEGORY = Collector.of(
                HashMap::new,
                (j, p) -> {
                    String name = p.getCategory().getName();
                    if (p.getCategory().getGroupId() == null) {
                        name = messageSource.getMessage(name, null, locale);
                    }

                    j.merge(name, p.getAmount().doubleValue(), (v1, v2) -> v1 + v2);
                },
                (j1, j2) -> {
                    j1.putAll(j2);
                    return j1;
                },
                x -> x);

        return expenses
                .stream()
                .collect(EXPENSE_COLLECTOR_BY_CATEGORY);
    }

    @Override
    @Transactional
    public List<PairDTO> getIncomesStatistics(@NotNull UUID groupId, @NotNull String periodUnit, @NotNull Integer periodLength, @NotNull Date toDate) {
        checkIfIsMember(groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND));
        Date fromDate = resolveDate(periodUnit, periodLength, toDate);
        List<Income> incomes = incomeRepository.findAllInRange(groupId, fromDate, toDate);

        return getIncomesInTimePeriod(incomes, periodUnit, periodLength, toDate)
                .entrySet()
                .stream()
                .sorted(COMPARATOR_BY_KEY)
                .map(ENTRY_PAIR_DTO_FUNCTION)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void registerSavingEntry(@NotNull SavingEntryDTO savingEntryDTO) {
        checkIfIsMember(groupRepository.findById(savingEntryDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND));
        Saving saving = groupRepository.findById(savingEntryDTO.getGroupId()).orElseThrow(ENTITY_NOT_FOUND).getSaving();
        SavingEntry savingEntry = savingEntryMapper.dtoToEntity(savingEntryDTO);

        savingEntry.setCurrentAmount(saving.getAmount().add(savingEntry.getAmountAdded()));
        savingEntry.setSaving(saving);
        saving.setAmount(savingEntry.getCurrentAmount());
        saving.getSavingEntries().add(savingEntry);

        savingRepository.save(saving);
    }

    @Override
    @Transactional
    public SavingStatisticsDTO getSavingEntryStatistics(@NotNull UUID groupId, @NotNull String periodUnit, @NotNull Integer periodLength, @NotNull Date toDate) {
        checkIfIsMember(groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND));
        Date fromDate = resolveDate(periodUnit, periodLength, toDate);
        Saving saving = savingRepository.findByGroupId(groupId);
        List<SavingEntry> savingEntries = savingEntryRepository.findAllInRange(saving.getId(), fromDate, toDate);
        Optional<SavingEntry> firstBeforeOpt = savingEntryRepository.findByDateCustom(fromDate, saving.getId());

        List<GoalDTO> goals = goalRepository
                .findAllBySavingId(saving.getId())
                .stream()
                .map(goalMapper::entityToDto)
                .collect(Collectors.toList());
        SavingStatisticsDTO result = new SavingStatisticsDTO();
        result.setGoals(goals);


        List<PairDTO> data = getSavingEntriesInTimePeriod(savingEntries, periodUnit, periodLength, toDate)
                .entrySet()
                .stream()
                .sorted(COMPARATOR_BY_KEY)
                .map(ENTRY_PAIR_DTO_FUNCTION)
                .collect(Collectors.toList());

        double lastVal = 0;
        if (firstBeforeOpt.isPresent()) {
            lastVal = firstBeforeOpt.get().getCurrentAmount().doubleValue();
        }

        for (PairDTO p : data) {
            p.setValue(p.getValue() + lastVal);

            lastVal = p.getValue();
        }


        result.setSavings(data);
        return result;
    }

    @Override
    @Transactional
    public FinanceStatisticsDTO getStatistics(@NotNull UUID groupId, @NotNull String periodUnit, @NotNull Integer periodLength, @NotNull Date toDate) {
        Group group = groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(group);

        Date fromDate = resolveDate(periodUnit, periodLength, toDate);

        List<Income> incomes = incomeRepository.findAllInRange(groupId, fromDate, toDate);
        List<Expense> expenses = expenseRepository.findAllInRange(groupId, fromDate, toDate);

        List<PairDTO> barChartData = resolveDailyBalance(incomes, expenses, periodUnit, periodLength, toDate);
        List<PairDTO> lineChartData = new ArrayList<>();
        double balance = 0;
        for (PairDTO overall : barChartData) {
            balance += overall.getValue();
        }

        balance = group.getBalance().doubleValue() - balance;

        for(PairDTO overall: barChartData){
            balance += overall.getValue();
            lineChartData.add(new PairDTO(overall.getDate(), balance));
        }

        FinanceStatisticsDTO dto = new FinanceStatisticsDTO();
        dto.setBarChartData(barChartData);
        dto.setLineChartData(lineChartData);

        return dto;
    }

    @Override
    public DailyStatisticsDTO getDailyStatistics(@NotNull UUID groupId, @NotNull Date date) {
        Group group = groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(group);

        return new DailyStatisticsDTO(
                incomeRepository.findAllByGroupIdAndDateCreated(groupId, date)
                        .stream().map(incomeMapper::entityToDto).collect(Collectors.toList()),
                expenseRepository.findAllByGroupIdAndDateCreated(groupId, date)
                        .stream().map(expenseMapper::entityToDto).collect(Collectors.toList()),
                goalRepository.findAllBySavingIdAndTargetDate(group.getSaving().getId(), date)
                        .stream().map(goalMapper::entityToDto).collect(Collectors.toList()));
    }

    @Override
    public CalendarStatisticsDTO getCalendarStatistics(UUID groupId, String periodUnit, Integer periodLength, Date toDate) {
        Group group = groupRepository.findById(groupId).orElseThrow(ENTITY_NOT_FOUND);
        checkIfIsMember(group);

        Date fromDate = resolveDate(periodUnit, periodLength, toDate);

        Long savingId = group.getSaving().getId();
        List<Income> incomes = incomeRepository.findAllInRange(groupId, fromDate, toDate);
        List<Expense> expenses = expenseRepository.findAllInRange(groupId, fromDate, toDate);
        List<Goal> goals = goalRepository.findAllInRange(savingId, fromDate, toDate);
        List<SavingEntry> savings = savingEntryRepository.findAllInRange(savingId, fromDate,toDate );

        List<PairDTO> dailyBalance = resolveDailyBalance(incomes, expenses, periodUnit, periodLength, toDate);
        List<PairDTO> goalsData = goals.stream()
                .map(goal -> new PairDTO(
                        FinanceServiceCollectors.YYYY_MM_DD.format(goal.getTargetDate()),
                        goal.getTargetAmount().doubleValue()))
                .collect(Collectors.toList());
        List<PairDTO> savingsData = savings.stream()
                .map(savingEntry -> new PairDTO(
                    FinanceServiceCollectors.YYYY_MM_DD.format(savingEntry.getDateCreated()),
                    savingEntry.getAmountAdded().doubleValue()))
                .collect(Collectors.toList());

        return new CalendarStatisticsDTO(dailyBalance, goalsData, savingsData);
    }

    private List<PairDTO> resolveDailyBalance(List<Income> incomes, List<Expense> expenses, String periodUnit, Integer periodLength, Date toDate) {
        Map<String, Double> incomesMap = getIncomesInTimePeriod(incomes, periodUnit, periodLength, toDate);
        Map<String, Double> expensesMap = getExpensesInTimePeriod(expenses, periodUnit, periodLength, toDate);

        Map<String, Double> dataMap = new HashMap<>(incomesMap);
        expensesMap.forEach((k, v) -> dataMap.merge(k, -v, (v1, v2) -> v1 + v2));

        return dataMap
                .entrySet().stream().sorted(COMPARATOR_BY_KEY).map(e -> new PairDTO(e.getKey(), e.getValue())).collect(Collectors.toList());
    }

    private Map<String, Double> getSavingEntriesInTimePeriod(List<SavingEntry> savingEntries, String timePeriod, int periodLength, Date toDate) {
        LocalDate localDate = Instant.ofEpochMilli(toDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        switch (timePeriod) {
            case "d":
                return resolveDatedPairs(timePeriod, periodLength, localDate, savingEntries.stream().collect(FinanceServiceCollectors.SAVING_ENTRIES_COLLECTOR_BY_DATE));
            case "m":
                return resolveDatedPairs(timePeriod, periodLength, localDate, savingEntries.stream().collect(FinanceServiceCollectors.SAVING_ENTRIES_COLLECTOR_BY_MONTH));
            case "y":
                return resolveDatedPairs(timePeriod, periodLength, localDate, savingEntries.stream().collect(FinanceServiceCollectors.SAVING_ENTRIES_COLLECTOR_BY_YEAR));
            default:
                throw new IllegalArgumentException("Illegal time period");
        }
    }

    private Map<String, Double> getExpensesInTimePeriod(List<Expense> expenses, String timePeriod, int periodLength, Date toDate) {
        LocalDate localDate = Instant.ofEpochMilli(toDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        switch (timePeriod) {
            case "d":
                return resolveDatedPairs(timePeriod, periodLength, localDate, expenses.stream().collect(FinanceServiceCollectors.EXPENSE_COLLECTOR_BY_DATE));
            case "m":
                return resolveDatedPairs(timePeriod, periodLength, localDate, expenses.stream().collect(FinanceServiceCollectors.EXPENSE_COLLECTOR_BY_MONTH));
            case "y":
                return resolveDatedPairs(timePeriod, periodLength, localDate,expenses.stream().collect(FinanceServiceCollectors.EXPENSE_COLLECTOR_BY_YEAR));
            default:
                throw new IllegalArgumentException("Illegal time period");
        }
    }

    private Map<String, Double> getIncomesInTimePeriod(List<Income> incomes, String timePeriod, int periodLength, Date toDate) {
        LocalDate localDate = Instant.ofEpochMilli(toDate.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
        switch (timePeriod) {
            case "d":
                return resolveDatedPairs(timePeriod, periodLength, localDate, incomes.stream().collect(FinanceServiceCollectors.INCOME_COLLECTOR_BY_DATE));
            case "m":
                return resolveDatedPairs(timePeriod, periodLength, localDate, incomes.stream().collect(FinanceServiceCollectors.INCOME_COLLECTOR_BY_MONTH));
            case "y":
                return resolveDatedPairs(timePeriod, periodLength, localDate, incomes.stream().collect(FinanceServiceCollectors.INCOME_COLLECTOR_BY_YEAR));
            default:
                throw new IllegalArgumentException("Illegal time period");
        }
    }
}
