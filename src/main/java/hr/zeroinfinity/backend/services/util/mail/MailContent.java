package hr.zeroinfinity.backend.services.util.mail;

public interface MailContent {

    /**
     * @param key   string which will be replaced in template
     * @param value string which will replace all the occurrences of the key
     */
    void setArgument(String key, String value);

    /**
     * @return text content of mail
     */
    String getContent();
}
