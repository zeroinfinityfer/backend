package hr.zeroinfinity.backend.services.util.notification;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import hr.zeroinfinity.backend.entities.Device;
import hr.zeroinfinity.backend.entities.User;
import hr.zeroinfinity.backend.services.util.notification.model.AndroidNotificationModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class CustomNotificationSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomNotificationSender.class);
    private static final Function<Device, String> GET_DEVICE_CODE = Device::getDeviceCode;
    private static final String TOPIC_MESSAGE = "Notification sent to topic %s";
    private static final String DEVICE_MESSAGE_SUCCESS = "Notification sent to device %s";
    private static final String DEVICE_MESSAGE_FAIL = "Notification hasn't been send to device %s";

    private final FirebaseApp firebaseApp;
    private final Executor executor;

    @Autowired
    public CustomNotificationSender(Executor executor, FirebaseApp firebaseApp) {
        this.firebaseApp = firebaseApp;
        this.executor = executor;
    }

    public void sendData(Map<String, String> data, String topic) {
        Message message = Message.builder()
                .setTopic(topic)
                .setAndroidConfig(AndroidConfig.builder()
                        .setTtl(3600 * 1000 * 24) // 1 hour in milliseconds
                        .setPriority(AndroidConfig.Priority.HIGH)
                        .putAllData(data)
                        .build())
                .build();

        FirebaseMessaging
                .getInstance(firebaseApp)
                .sendAsync(message)
                .addListener(() -> LOGGER.info(String.format(TOPIC_MESSAGE, topic)), executor);
    }

    public void sendData(Map<String, String> data, User user) {
        if (user.getDevices() == null || user.getDevices().size() == 0) return;

        List<String> deviceCodes = user.getDevices().stream().map(GET_DEVICE_CODE).collect(Collectors.toList());
        CompletableFuture.supplyAsync(() -> {
            Message.Builder builder = Message.builder()
                    .setAndroidConfig(AndroidConfig.builder()
                            .setTtl(3600 * 1000 * 24) // 1 hour in milliseconds
                            .setPriority(AndroidConfig.Priority.HIGH)
                            .putAllData(data)
                            .build());

            deviceCodes.forEach(code -> sendToDevice(code, builder));
            return null;
        }, executor);
    }

    public void sendNotification(AndroidNotificationModel notification, String topic) {
        Message message = Message.builder()
                .setTopic(topic)
                .setAndroidConfig(AndroidConfig.builder()
                        .setTtl(3600 * 1000 * 24) // 1 hour in milliseconds
                        .setPriority(AndroidConfig.Priority.NORMAL)
                        .setNotification(AndroidNotification.builder()
                                .setClickAction(notification.getClickAction())
                                .setTitle(notification.getTitle())
                                .setBody(notification.getBody())
                                .setIcon(notification.getIcon())
                                .setColor(notification.getColorCode())
                                .build())
                        .build())
                .build();

        FirebaseMessaging
                .getInstance(firebaseApp)
                .sendAsync(message)
                .addListener(
                        () -> LOGGER.info(String.format(TOPIC_MESSAGE, topic))
                        , executor);
    }

    public void sendNotification(AndroidNotificationModel notification, User user) {
        if (user.getDevices() == null || user.getDevices().size() == 0) return;

        List<String> deviceCodes = user.getDevices().stream().map(GET_DEVICE_CODE).collect(Collectors.toList());
        CompletableFuture.supplyAsync(() -> {
            Message.Builder builder = Message.builder()
                    .setAndroidConfig(AndroidConfig.builder()
                            .setTtl(3600 * 1000 * 24) // 1 hour in milliseconds
                            .setPriority(AndroidConfig.Priority.NORMAL)
                            .setNotification(AndroidNotification.builder()
                                    .setClickAction(notification.getClickAction())
                                    .setTitle(notification.getTitle())
                                    .setBody(notification.getBody())
                                    .setIcon(notification.getIcon())
                                    .setColor(notification.getColorCode())
                                    .build())
                            .build());

            deviceCodes.forEach(
                    code -> sendToDevice(code, builder));
            return null;
        }, executor);
    }

    private void sendToDevice(String code, Message.Builder builder) {
        try {
            FirebaseMessaging.getInstance(firebaseApp).send(builder.setToken(code).build());
            LOGGER.info(String.format(DEVICE_MESSAGE_SUCCESS, code));
        } catch (FirebaseMessagingException ex) {
            LOGGER.warn(String.format(DEVICE_MESSAGE_FAIL, code));
        }
    }
}
