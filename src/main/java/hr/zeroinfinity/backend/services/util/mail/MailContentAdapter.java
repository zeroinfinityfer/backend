package hr.zeroinfinity.backend.services.util.mail;

import hr.zeroinfinity.backend.common.files.CustomTextFileReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class MailContentAdapter implements MailContent {

    private String template;
    private ThreadLocal<Map<String, String>> ARGUMENTS = new ThreadLocal<>();

    protected MailContentAdapter() {
    }

    protected void setResource(InputStream inputStream) throws IOException {
        this.template = CustomTextFileReader.readFromInputStream(inputStream);
    }

    @Override
    public void setArgument(String key, String value) {
        Map<String, String> argumentMap = ARGUMENTS.get();
        if (argumentMap == null) {
            argumentMap = new HashMap<>();
            ARGUMENTS.set(argumentMap);
        }

        argumentMap.put(key, value);
    }

    @Override
    public String getContent() {
        String text = template;
        Map<String, String> argumentsMap = ARGUMENTS.get();
        if (argumentsMap == null) return text;

        for (Map.Entry<String, String> entry : argumentsMap.entrySet()) {
            text = text.replaceAll(entry.getKey(), entry.getValue());
        }

        ARGUMENTS.get().clear();
        return text;
    }
}