package hr.zeroinfinity.backend.services.util;

import hr.zeroinfinity.backend.entities.Expense;
import hr.zeroinfinity.backend.entities.Income;
import hr.zeroinfinity.backend.entities.SavingEntry;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collector;

public class FinanceServiceCollectors {
    public static final DateFormat YYYY_MM = new SimpleDateFormat("yyyy-MM");
    public static final DateFormat YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");

    public static final Collector<Expense, Map<String, Double>, Map<String, Double>> EXPENSE_COLLECTOR_BY_DATE = Collector.of(
            HashMap::new,
            (j, p) -> j.merge(YYYY_MM_DD.format(p.getDateCreated()), p.getAmount().doubleValue(), (v1, v2) -> v1 + v2),
            (j1, j2) -> {
                j1.putAll(j2);
                return j1;
            },
            x -> x);

    public static final Collector<Expense, Map<String, Double>, Map<String, Double>> EXPENSE_COLLECTOR_BY_MONTH = Collector.of(
            HashMap::new,
            (j, p) -> j.merge(YYYY_MM_DD.format(p.getDateCreated()), p.getAmount().doubleValue(), (v1, v2) -> v1 + v2),
            (j1, j2) -> {
                j1.putAll(j2);
                return j1;
            },
            x -> x);

    public static final Collector<Expense, Map<String, Double>, Map<String, Double>> EXPENSE_COLLECTOR_BY_YEAR = Collector.of(
            HashMap::new,
            (j, p) -> j.merge(YYYY_MM.format(p.getDateCreated()), p.getAmount().doubleValue(), (v1, v2) -> v1 + v2),
            (j1, j2) -> {
                j1.putAll(j2);
                return j1;
            },
            x -> x);

    public static final Collector<Income, Map<String, Double>, Map<String, Double>> INCOME_COLLECTOR_BY_DATE = Collector.of(
            HashMap::new,
            (j, p) -> j.merge(YYYY_MM_DD.format(p.getDateCreated()), p.getAmount().doubleValue(), (v1, v2) -> v1 + v2),
            (j1, j2) -> {
                j1.putAll(j2);
                return j1;
            },
            x -> x);

    public static final Collector<Income, Map<String, Double>, Map<String, Double>> INCOME_COLLECTOR_BY_MONTH = Collector.of(
            HashMap::new,
            (j, p) -> j.merge(YYYY_MM_DD.format(p.getDateCreated()), p.getAmount().doubleValue(), (v1, v2) -> v1 + v2),
            (j1, j2) -> {
                j1.putAll(j2);
                return j1;
            },
            x -> x);

    public static final Collector<Income, Map<String, Double>, Map<String, Double>> INCOME_COLLECTOR_BY_YEAR = Collector.of(
            HashMap::new,
            (j, p) -> j.merge(YYYY_MM.format(p.getDateCreated()), p.getAmount().doubleValue(), (v1, v2) -> v1 + v2),
            (j1, j2) -> {
                j1.putAll(j2);
                return j1;
            },
            x -> x);

    public static final Collector<SavingEntry, Map<String, Double>, Map<String, Double>> SAVING_ENTRIES_COLLECTOR_BY_DATE = Collector.of(
            HashMap::new,
            (j, p) -> j.merge(YYYY_MM_DD.format(p.getDateCreated()), p.getAmountAdded().doubleValue(), (v1, v2) -> v1 + v2)
            , (j1, j2) -> {
                j1.putAll(j2);
                return j1;
            },
            x -> x
    );

    public static final Collector<SavingEntry, Map<String, Double>, Map<String, Double>> SAVING_ENTRIES_COLLECTOR_BY_MONTH = Collector.of(
            HashMap::new,
            (j, p) -> j.merge(YYYY_MM_DD.format(p.getDateCreated()), p.getAmountAdded().doubleValue(), (v1, v2) -> v1 + v2),
            (j1, j2) -> {
                j1.putAll(j2);
                return j1;
            },
            x -> x
    );

    public static final Collector<SavingEntry, Map<String, Double>, Map<String, Double>> SAVING_ENTRIES_COLLECTOR_BY_YEAR = Collector.of(
            HashMap::new,
            (j, p) -> j.merge(YYYY_MM.format(p.getDateCreated()), p.getAmountAdded().doubleValue(), (v1, v2) -> v1 + v2)
            , (j1, j2) -> {
                j1.putAll(j2);
                return j1;
            }, x -> x);
}
