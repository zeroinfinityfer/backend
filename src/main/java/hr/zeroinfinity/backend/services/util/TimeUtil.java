package hr.zeroinfinity.backend.services.util;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class TimeUtil {
    private static final DateTimeFormatter LOCAL_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter LOCAL_DATE_FORMAT_MONTHS = DateTimeFormatter.ofPattern("yyyy-MM");
    private static final IllegalArgumentException ILLEGAL_TIME_PERIOD = new IllegalArgumentException("Illegal time period");

    public static Map<String, Double> resolveDatedPairs(String timeUnit, int periodLength, LocalDate currentDate, Map<String, Double> valuesMap){
        if (timeUnit.equals("y")) {
            LocalDate fromDate = currentDate.minusYears(periodLength);

            while (fromDate.isBefore(currentDate) || fromDate.isEqual(currentDate)) {
                String dateToString = fromDate.format(LOCAL_DATE_FORMAT_MONTHS);
                valuesMap.computeIfAbsent(dateToString, date -> (double) 0);

                fromDate = fromDate.plusMonths(1);
            }
        } else {
            LocalDate fromDate;

            switch (timeUnit) {
                case "m":
                    fromDate = currentDate.minusMonths(periodLength);
                    break;
                case "d":
                    fromDate = currentDate.minusDays(periodLength - 1);
                    break;
                default:
                    throw ILLEGAL_TIME_PERIOD;
            }

            while (fromDate.isBefore(currentDate) || fromDate.isEqual(currentDate)) {
                String dateToString = fromDate.format(LOCAL_DATE_FORMAT);
                valuesMap.computeIfAbsent(dateToString, date -> (double) 0);

                fromDate = fromDate.plusDays(1L);
            }
        }

        return valuesMap;
    }

    public static Date resolveDate(@NotNull String periodUnit, @NotNull Integer periodLength, Date toDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(toDate);

        if (periodLength <= 0) {
            throw ILLEGAL_TIME_PERIOD;
        }

        switch (periodUnit) {
            case "d":
                cal.add(Calendar.DATE, - periodLength);
                break;
            case "m":
                cal.add(Calendar.MONTH, - periodLength);
                break;
            case "y":
                cal.add(Calendar.YEAR, -periodLength);
                break;
            default:
                throw ILLEGAL_TIME_PERIOD;
        }


        return cal.getTime();
    }
}
