package hr.zeroinfinity.backend.services.util.notification.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;

@Setter(value = AccessLevel.PROTECTED)
@Getter
public class AndroidNotificationModel {
    private static final String DEFAULT_ICON = "ic_notification";
    private static final String EMPTY = "";
    private static final String COLOR_FORMAT = "#%02x%02x%02x";

    private String clickAction = DEFAULT_ICON;
    private String title = EMPTY;
    private String body = EMPTY;
    private String icon = DEFAULT_ICON;

    @Getter(value = AccessLevel.PRIVATE)
    private Color color = Color.WHITE;

    public String getColorCode() {
        return String.format(COLOR_FORMAT,
                color.getRed(),
                color.getGreen(),
                color.getBlue());
    }
}
