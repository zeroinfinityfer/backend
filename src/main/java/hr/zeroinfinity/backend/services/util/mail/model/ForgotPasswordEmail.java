package hr.zeroinfinity.backend.services.util.mail.model;

import hr.zeroinfinity.backend.services.util.mail.MailContentAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class ForgotPasswordEmail extends MailContentAdapter {

    @Value("classpath:/static/forgot-password.html")
    private Resource templateResource;


    public ForgotPasswordEmail() {
    }

    @PostConstruct
    private void init() {
        try {
            super.setResource(templateResource.getInputStream());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }
}
