package hr.zeroinfinity.backend.services.util.notification.model;

public class InviteNotificationModel extends AndroidNotificationModel {
    private static final String DEFAULT_MESSAGE = "User %s invited you to a group %s. Join!";
    private static final String DEFAULT_TITLE = "Invite";
    private static final String DEFAULT_CLICK_ACTION = "hr.zeroinfinity.notification.FCM_INVITE";

    public InviteNotificationModel(String adminEmail, String groupName) {
        super();

        super.setBody(String.format(DEFAULT_MESSAGE,adminEmail, groupName));
        super.setTitle(DEFAULT_TITLE);
        super.setClickAction(DEFAULT_CLICK_ACTION);
    }
}
