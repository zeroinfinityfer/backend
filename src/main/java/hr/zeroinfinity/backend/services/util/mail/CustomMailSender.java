package hr.zeroinfinity.backend.services.util.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Component
public class CustomMailSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomMailSender.class);
    private static final String SUCCESS_MESSAGE = "Mail has been send successfully to %s";
    private static final String FAILED_MESSAGE = "Mail has failed to be sent to %s";

    private static final ThreadLocal<CustomMail> MAIL_MAP = new ThreadLocal<>();

    private final JavaMailSender javaMailSender;
    private final Executor executor;

    @Autowired
    private CustomMailSender(JavaMailSender javaMailSender, Executor executor) {
        this.javaMailSender = javaMailSender;
        this.executor = executor;
    }

    /**
     * @return new CustomMail object
     */
    public CustomMail create() {
        CustomMail customMail = MAIL_MAP.get();
        if (customMail == null) {
            customMail = new CustomMail();
            MAIL_MAP.set(customMail);
        }

        return customMail;
    }

    /**
     * Sends mail asynchronously.
     *
     * @param message message to be send
     * @param to      destination
     * @return future object
     */
    private CompletableFuture<Boolean> sendMailAsync(MimeMessage message, String to) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                javaMailSender.send(message);
            } catch (MailException ex) {
                LOGGER.info(String.format(FAILED_MESSAGE, to));
                return false;
            }

            LOGGER.info(String.format(SUCCESS_MESSAGE, to));
            return true;
        }, executor);
    }

    /**
     * Custom mail class. Contains information necessary for sending email.
     */
    public class CustomMail {
        private static final String RETRY_MESSAGE = "Retrying to send mail to %s.";
        private static final String ERROR_FORMAT = "Fields not ready; from: %s, to: %s, subject: %s, content: %s";

        private String from;
        private String to;
        private String subject;
        private short sendCounter = 10;

        private CustomMail() {
        }

        public CustomMail setFrom(String from) {
            this.from = from;
            return this;
        }

        public CustomMail setTo(String to) {
            this.to = to;
            return this;
        }

        public CustomMail setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public void send(MailContent content, boolean isHtml) throws MessagingException {
            if (from == null || to == null || subject == null || content == null) {
                reset();
                throw new MessagingException(String.format(ERROR_FORMAT, from, to, subject, content == null));
            }

            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(this.from);
            helper.setTo(this.to);
            helper.setSubject(this.subject);
            helper.setText(content.getContent(), isHtml);

            sendMail(message, this.to);
            reset();

        }

        private void sendMail(MimeMessage message, String to) {
            sendMailAsync(message, to).thenAccept(result -> {
                if (!result) {
                    if (sendCounter > 0) {
                        sendCounter--;

                        LOGGER.info(String.format(RETRY_MESSAGE, to));
                        sendMail(message, to);
                    }
                }
            });
        }

        private void reset() {
            this.from = null;
            this.to = null;
            this.subject = null;
            this.sendCounter = 10;
        }
    }
}
