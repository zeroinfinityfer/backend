package hr.zeroinfinity.backend.services.util.mail.model;

import hr.zeroinfinity.backend.services.util.mail.MailContentAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class InviteSenderEmail extends MailContentAdapter {

    @Value("classpath:/static/invite-send.html")
    private Resource templateResource;

    public InviteSenderEmail() {
    }

    @PostConstruct
    private void init() {
        try {
            super.setResource(templateResource.getInputStream());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }
}
