package hr.zeroinfinity.backend.services;

import hr.zeroinfinity.backend.dto.GroupAdminDTO;
import hr.zeroinfinity.backend.dto.GroupAdminRemoveDTO;
import hr.zeroinfinity.backend.dto.GroupDTO;
import hr.zeroinfinity.backend.dto.GroupInviteDTO;
import hr.zeroinfinity.backend.dto.GroupMemberInviteDTO;
import hr.zeroinfinity.backend.dto.GroupMemberRemoveDTO;
import hr.zeroinfinity.backend.dto.InviteDTO;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public interface GroupService {

    /**
     * @param groupDTO information for group to be registered
     * @return entity id
     */
    UUID registerGroup(GroupDTO groupDTO, Locale locale) throws MessagingException;

    /**
     * @param groupDTO information for group to be edited
     */
    void editGroup(GroupDTO groupDTO);


    /**
     * @param inviteId id of invite which will be accepted
     */
    boolean acceptInvite(Long inviteId);

    /**
     * @param inviteId id of invite which will be declined
     */
    void declineInvite(Long inviteId);

    /**
     * @return list of groups for active user
     */
    List<GroupDTO> getGroupsOfCurrentUser(Locale locale);

    /**
     * @param groupInviteDTO object holding emails of future members
     * @throws MessagingException if
     */
    void addMembersToGroup(GroupInviteDTO groupInviteDTO) throws MessagingException;

    /**
     * @param groupMemberInviteDTO object holds email of future member
     */
    void addSingleMemberToGroup(GroupMemberInviteDTO groupMemberInviteDTO) throws MessagingException;


    /**
     * @param groupAdminDTO holds future Admin id and id of the group to which he will become admin
     */
    void addSingleAdmin(GroupAdminDTO groupAdminDTO);

    /**
     * @param groupMemberRemoveDTO holds userId and groupId of user which will be removed from group with groupId.
     */
    void removeMember(GroupMemberRemoveDTO groupMemberRemoveDTO);

    /**
     * @param groupId is id of group that will be deleted.
     */
    void deleteGroup(UUID groupId);

    /**
     * @param groupAdminRemoveDTO holds groupId of group from which current user will be
     *                            dismissed from admin role.
     */
    void removeAdmin(GroupAdminRemoveDTO groupAdminRemoveDTO);

    /**
     * @return list of pending group invites
     */
    List<InviteDTO> getInvitesOfCurrentUser();
}
