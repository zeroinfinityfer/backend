package hr.zeroinfinity.backend.services;

import hr.zeroinfinity.backend.dto.CalendarStatisticsDTO;
import hr.zeroinfinity.backend.dto.CategoryDTO;
import hr.zeroinfinity.backend.dto.DailyStatisticsDTO;
import hr.zeroinfinity.backend.dto.ExpenseDTO;
import hr.zeroinfinity.backend.dto.FinanceStatisticsDTO;
import hr.zeroinfinity.backend.dto.GoalDTO;
import hr.zeroinfinity.backend.dto.IncomeDTO;
import hr.zeroinfinity.backend.dto.PairDTO;
import hr.zeroinfinity.backend.dto.SavingEntryDTO;
import hr.zeroinfinity.backend.dto.SavingStatisticsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public interface FinanceService {

    /**
     * @param categoryDTO information for category to be registered
     * @return id of created entity
     */
    Long registerCategory(CategoryDTO categoryDTO);

    /**
     * @param expenseDTO information for expense to be registered
     * @return id of created entity
     */
    Long registerExpense(ExpenseDTO expenseDTO);

    /**
     * @param goalDTO information for goal to be registered
     * @return id of created entity
     */
    Long registerGoal(GoalDTO goalDTO);

    /**
     * @param incomeDTO information for income to be registered
     * @return id of created entity
     */
    Long registerIncome(IncomeDTO incomeDTO);

    /**
     * @param expenseId id of expense to be removed
     */
    void deleteExpense(Long expenseId);

    /**
     * @param categoryId id of category to be removed
     */
    void deleteCategory(Long categoryId);

    /**
     * @param goalId id of goal to be removed
     */
    void deleteGoal(Long goalId);

    /**
     * @param incomeId id of income to be removed
     */
    void deleteIncome(Long incomeId);

    /**
     * @param groupId group id
     * @return list of categories
     */
    List<CategoryDTO> getCategoriesByGroupId(UUID groupId, Locale locale);

    /**
     * @param groupId  group id
     * @param pageable pageable object
     * @return list of expenses pageable
     */
    Page<ExpenseDTO> getExpensesByGroupIdPageable(UUID groupId, Pageable pageable);

    /**
     * @param groupId  group id
     * @param pageable pageable object
     * @return list of goals pageable
     */
    Page<GoalDTO> getGoalsByGroupIdPageable(UUID groupId, Pageable pageable);

    /**
     * @param groupId  group id
     * @param pageable pageable object
     * @return list of incomes pageable
     */
    Page<IncomeDTO> getIncomesByGroupIdPageable(UUID groupId, Pageable pageable);

    /**
     * @param expenseDTO information for expense to be edited
     */
    void editExpense(ExpenseDTO expenseDTO);

    /**
     * @param goalDTO information for goal to be edited
     */
    void editGoal(GoalDTO goalDTO);

    /**
     * @param incomeDTO information for income to be edited
     */
    void editIncome(IncomeDTO incomeDTO);

    /**
     * @param categoryDTO information for category to be edited
     */
    void editCategory(CategoryDTO categoryDTO);

    /**
     * @param goalId goal id
     * @param locale context locale
     * @return Boolean status of approval
     */
    Boolean approveGoal(Long goalId, Locale locale, Date today);


    /**
     * @param groupId      group id
     * @param periodUnit   period unit (d - days, m - months, y - years)
     * @param periodLength period length (positive number)
     * @return pie chart data
     */
    Map<String, Double> getExpensesStatistics(@NotNull UUID groupId, @NotNull String periodUnit, @NotNull Integer periodLength, Locale locale, @NotNull Date toDate);


    /**
     * @param groupId      group id
     * @param periodUnit   period unit (d - days, m - months, y - years)
     * @param periodLength period length (positive number)
     * @return chart data
     */
    List<PairDTO> getIncomesStatistics(@NotNull UUID groupId, @NotNull String periodUnit, @NotNull Integer periodLength, @NotNull Date toDate);

    /**
     * @param savingEntryDTO saving entry to be registered
     */
    void registerSavingEntry(@NotNull SavingEntryDTO savingEntryDTO);

    /**
     * @param groupId      group id
     * @param periodUnit   period unit (d - days, m - months, y - years)
     * @param periodUnit   period unit (d - days, m - months, y - years)
     * @param periodLength period length (positive number)
     * @return chart data
     */
    SavingStatisticsDTO getSavingEntryStatistics(@NotNull UUID groupId, @NotNull String periodUnit, @NotNull Integer periodLength, @NotNull Date toDate);

    /**
     * @param groupId      group id
     * @param periodUnit   period unit (d - days, m - months, y - years)
     * @param periodLength period length (positive number)
     * @param toDate       to which date
     * @return combined chart data
     */
    FinanceStatisticsDTO getStatistics(@NotNull UUID groupId, @NotNull String periodUnit, @NotNull Integer periodLength, @NotNull Date toDate);

    /**
     * @param groupId group id
     * @param date    selected date
     * @return daily statistics object containing:
     * <ul>
     * <li>expenses</li>
     * <li>incomes</li>
     * <li>incoming goal deadlines</li>
     * </ul>
     */
    DailyStatisticsDTO getDailyStatistics(@NotNull UUID groupId, @NotNull Date date);

    /**
     * @param groupId      group id
     * @param periodUnit   period unit (d - days, m - months, y - years)
     * @param periodLength period length (positive number)
     * @param toDate       to which date
     * @return calendar statistics object
     */
    CalendarStatisticsDTO getCalendarStatistics(UUID groupId, String periodUnit, Integer periodLength, Date toDate);
}
