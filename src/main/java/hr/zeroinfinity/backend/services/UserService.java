package hr.zeroinfinity.backend.services;

import hr.zeroinfinity.backend.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.mail.MessagingException;
import javax.validation.constraints.NotNull;
import java.util.Locale;
import java.util.UUID;

public interface UserService extends UserDetailsService {

    /**
     * @param id users unique identifier
     * @return user with provided id, null if no user is found
     */
    UserDTO getUserById(@NotNull UUID id);

    /**
     * @param email users email
     * @return user with provided email
     */
    UserDTO getUserByEmail(@NotNull String email);

    /**
     * @param user information for user to be registered.
     */
    void registerUser(@NotNull UserDTO user, @NotNull Locale locale) throws MessagingException;

    /**
     * @param email email of user which password is reset
     */
    void resetPassword(String email) throws MessagingException;

    /**
     * Verifies user registration.
     *
     * @param userId user unique id
     */
    void verifyUser(UUID userId);

    /**
     * @param deviceCode id of device to be registered
     */
    void registerDevice(String deviceCode);

    /**
     * @param deviceCode id of device to be removed
     */
    void removeDevice(String deviceCode);

    /**
     * @param enabled is personal group enabled
     */
    void changePersonalGroupState(boolean enabled);
}
