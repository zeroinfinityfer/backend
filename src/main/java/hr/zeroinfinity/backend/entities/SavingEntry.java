package hr.zeroinfinity.backend.entities;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@Table(name = "SAVING_ENTRIES")
public class SavingEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(value = AccessLevel.PRIVATE)
    @Column(name = "id")
    private Long id;

    @Column(name = "SAVINGS_ID", updatable = false, insertable = false)
    private Long savingId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SAVINGS_ID", nullable = false)
    private Saving saving;

    @Column(name = "AMOUNT_ADDED", nullable = false)
    private BigDecimal amountAdded;

    @Column(name = "CURRENT_AMOUNT", nullable = false)
    private BigDecimal currentAmount;

    @Column(name = "DATE_CREATED", nullable = false)
    private Date dateCreated;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SavingEntry saving = (SavingEntry) o;
        return Objects.equals(id, saving.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SavingEntry{");
        sb.append("id=").append(id);
        sb.append(", savingId=").append(savingId);
        sb.append(", amountAdded=").append(amountAdded);
        sb.append(", currentAmount=").append(currentAmount);
        sb.append(", dateCreated=").append(dateCreated);
        sb.append('}');
        return sb.toString();
    }
}
