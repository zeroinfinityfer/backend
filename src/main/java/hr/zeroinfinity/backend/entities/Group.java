package hr.zeroinfinity.backend.entities;


import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@Table(name = "GROUPS")
public class Group implements Serializable {

    private static final long serialVersionUID = -8179372235529889630L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    @Column(name = "id", updatable = false, nullable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private UUID id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "CURRENCY", nullable = false)
    private String currency;

    @JoinTable(
            name = "GROUP_MEMBERS",
            joinColumns = @JoinColumn(
                    name = "GROUP_ID",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "USER_ID",
                    referencedColumnName = "id"
            )
    )
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<User> members;

    @JoinTable(
            name = "GROUP_ADMINS",
            joinColumns = @JoinColumn(
                    name = "GROUP_ID",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "USER_ID",
                    referencedColumnName = "id"
            )
    )
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<User> admins;

    @OneToMany(mappedBy = "group", orphanRemoval = true)
    private List<Expense> expenses;

    @OneToMany(mappedBy = "group", orphanRemoval = true)
    private List<Income> incomes;

    @OneToMany(mappedBy = "group", orphanRemoval = true)
    private List<Category> categories;

    @OneToOne(mappedBy = "group", orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Saving saving;

    @Column(name = "PERSONAL", updatable = false, nullable = false)
    private Boolean personal;

    @Column(name = "BALANCE", nullable = false)
    private BigDecimal balance;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(id, group.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Group{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", currency='").append(currency).append('\'');
        sb.append(", balance='").append(balance).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
