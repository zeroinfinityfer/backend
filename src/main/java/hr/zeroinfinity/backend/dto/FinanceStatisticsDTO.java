package hr.zeroinfinity.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class FinanceStatisticsDTO implements Serializable {
    private static final long serialVersionUID = -2223528511235310373L;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private List<PairDTO> lineChartData;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private List<PairDTO> barChartData;
}
