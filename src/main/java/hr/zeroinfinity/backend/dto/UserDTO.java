package hr.zeroinfinity.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class UserDTO implements Serializable {
    private static final long serialVersionUID = 2298183553129038183L;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private UUID id;

    private String name;

    private String surname;

    private String email;

    private String currency;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Setter(value = AccessLevel.PRIVATE)
    private String password;

    @JsonProperty(required = true)
    private Boolean personalGroupEnabled;
}
