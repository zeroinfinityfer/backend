package hr.zeroinfinity.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalendarStatisticsDTO implements Serializable {

    private static final long serialVersionUID = -2596913320019720638L;

    private List<PairDTO> dailyBalance;

    private List<PairDTO> goals;

    private List<PairDTO> savings;
}
