package hr.zeroinfinity.backend.dto.mappers.impl;

import hr.zeroinfinity.backend.dto.SavingEntryDTO;
import hr.zeroinfinity.backend.dto.mappers.Mapper;
import hr.zeroinfinity.backend.entities.SavingEntry;
import org.springframework.stereotype.Component;

@Component
public class SavingEntryMapper implements Mapper<SavingEntry, SavingEntryDTO> {

    @Override
    public SavingEntry dtoToEntity(SavingEntryDTO dto) {
        SavingEntry entity = new SavingEntry();
        entity.setAmountAdded(dto.getAmountAdded());
        entity.setDateCreated(dto.getDateCreated());

        return entity;
    }

    @Override
    public SavingEntryDTO entityToDto(SavingEntry entity) {
        SavingEntryDTO dto = new SavingEntryDTO();

        dto.setAmountAdded(entity.getAmountAdded());
        dto.setCurrentAmount(entity.getCurrentAmount());
        dto.setId(entity.getId());
        dto.setDateCreated(entity.getDateCreated());

        return dto;
    }
}
