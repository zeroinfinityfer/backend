package hr.zeroinfinity.backend.dto.mappers.impl;

import hr.zeroinfinity.backend.dto.ExpenseDTO;
import hr.zeroinfinity.backend.dto.mappers.Mapper;
import hr.zeroinfinity.backend.entities.Expense;
import hr.zeroinfinity.backend.repositories.CategoryRepository;
import hr.zeroinfinity.backend.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.function.Supplier;

@Component
public class ExpenseMapper implements Mapper<Expense, ExpenseDTO> {

    private static final Supplier<EntityNotFoundException> ENTITY_NOT_FOUND = EntityNotFoundException::new;
    private final GroupRepository groupRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public ExpenseMapper(@Lazy GroupRepository groupRepository, @Lazy CategoryRepository categoryRepository) {
        this.groupRepository = groupRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Expense dtoToEntity(ExpenseDTO dto) {
        Expense entity = new Expense();

        entity.setAmount(dto.getAmount());
        entity.setDateCreated(dto.getDateCreated());
        entity.setDescription(dto.getDescription());
        entity.setGroup(groupRepository.findById(dto.getGroupId()).orElseThrow(ENTITY_NOT_FOUND));
        entity.setCategory(categoryRepository.findById(dto.getCategoryId()).orElseThrow(ENTITY_NOT_FOUND));

        return entity;
    }

    @Override
    public ExpenseDTO entityToDto(Expense entity) {
        ExpenseDTO dto = new ExpenseDTO();

        dto.setAmount(entity.getAmount());
        dto.setDateCreated(entity.getDateCreated());
        dto.setCategoryId(entity.getCategoryId());
        dto.setDescription(entity.getDescription());
        dto.setGroupId(entity.getGroupId());
        dto.setId(entity.getId());

        return dto;
    }
}
