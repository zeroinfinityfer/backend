package hr.zeroinfinity.backend.dto.mappers.impl;

import hr.zeroinfinity.backend.dto.InviteDTO;
import hr.zeroinfinity.backend.dto.mappers.Mapper;
import hr.zeroinfinity.backend.entities.Invite;
import hr.zeroinfinity.backend.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.function.Supplier;

@Component
public class InviteMapper implements Mapper<Invite, InviteDTO> {

    private static final Supplier<EntityNotFoundException> ENTITY_NOT_FOUND_EXCEPTION_SUPPLIER = EntityNotFoundException::new;

    private final GroupRepository groupRepository;
    private final UserMapper userMapper;

    @Autowired
    public InviteMapper(GroupRepository groupRepository, UserMapper userMapper) {
        this.groupRepository = groupRepository;
        this.userMapper = userMapper;
    }

    @Override
    public Invite dtoToEntity(InviteDTO dto) {
        Invite invite = new Invite();
        invite.setCreatedAt(dto.getCreatedAt());
        invite.setEmail(dto.getEmail());
        invite.setGroupId(dto.getGroupId());

        return invite;
    }

    @Override
    public InviteDTO entityToDto(Invite entity) {
        InviteDTO inviteDTO = new InviteDTO();
        
        inviteDTO.setId(entity.getId());
        inviteDTO.setCreatedAt(entity.getCreatedAt());
        inviteDTO.setEmail(entity.getEmail());
        inviteDTO.setGroupId(entity.getGroupId());
        inviteDTO.setGroupName(groupRepository.findById(entity.getGroupId()).orElseThrow(ENTITY_NOT_FOUND_EXCEPTION_SUPPLIER).getName());
        inviteDTO.setInvitedBy(userMapper.entityToDto(entity.getInvitedBy()));

        return inviteDTO;
    }
}
