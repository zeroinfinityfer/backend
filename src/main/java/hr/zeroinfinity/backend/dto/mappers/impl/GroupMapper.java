package hr.zeroinfinity.backend.dto.mappers.impl;

import hr.zeroinfinity.backend.dto.GroupDTO;
import hr.zeroinfinity.backend.dto.UserDTO;
import hr.zeroinfinity.backend.dto.mappers.Mapper;
import hr.zeroinfinity.backend.entities.Group;
import hr.zeroinfinity.backend.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class GroupMapper implements Mapper<Group, GroupDTO> { //todo

    private final UserMapper userMapper;

    @Autowired
    public GroupMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }


    @Override
    public Group dtoToEntity(GroupDTO groupDTO) {
        Group group = new Group();

        group.setName(groupDTO.getName());
        group.setDescription(groupDTO.getDescription());
        group.setPersonal(false);

        return group;
    }

    @Override
    public GroupDTO entityToDto(Group entity) {
        GroupDTO groupDTO = new GroupDTO();

        List<UserDTO> membersDTOs = new ArrayList<>();
        List<User> members = entity.getMembers();
        for (User user : members) {
            membersDTOs.add(userMapper.entityToDto(user));
        }

        List<UserDTO> adminsDTOs = new ArrayList<>();
        List<User> admins = entity.getAdmins();
        for (User user : admins) {
            adminsDTOs.add(userMapper.entityToDto(user));
        }

        groupDTO.setId(entity.getId());
        groupDTO.setName(entity.getName());
        groupDTO.setDescription(entity.getDescription());
        groupDTO.setMembers(membersDTOs);
        groupDTO.setAdmins(adminsDTOs);
        groupDTO.setPersonal(entity.getPersonal());
        groupDTO.setCurrency(entity.getCurrency());
        groupDTO.setSavingsAmount(entity.getSaving().getAmount());
        groupDTO.setBalance(entity.getBalance());

        return groupDTO;
    }

}
