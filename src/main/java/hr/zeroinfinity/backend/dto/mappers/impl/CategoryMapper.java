package hr.zeroinfinity.backend.dto.mappers.impl;

import hr.zeroinfinity.backend.dto.CategoryDTO;
import hr.zeroinfinity.backend.dto.mappers.Mapper;
import hr.zeroinfinity.backend.entities.Category;
import hr.zeroinfinity.backend.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.function.Supplier;

@Component
public class CategoryMapper implements Mapper<Category, CategoryDTO> {

    private static final Supplier<EntityNotFoundException> ENTITY_NOT_FOUND = EntityNotFoundException::new;
    private final GroupRepository groupRepository;

    @Autowired
    public CategoryMapper(@Lazy GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public Category dtoToEntity(CategoryDTO dto) {
        Category entity = new Category();

        entity.setName(dto.getName());
        entity.setGroup(groupRepository.findById(dto.getGroupId()).orElseThrow(ENTITY_NOT_FOUND));

        return entity;
    }

    @Override
    public CategoryDTO entityToDto(Category entity) {
        CategoryDTO dto = new CategoryDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());

        return dto;
    }
}
