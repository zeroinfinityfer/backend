package hr.zeroinfinity.backend.dto.mappers.impl;

import hr.zeroinfinity.backend.dto.GoalDTO;
import hr.zeroinfinity.backend.dto.mappers.Mapper;
import hr.zeroinfinity.backend.entities.Goal;
import hr.zeroinfinity.backend.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.function.Supplier;

@Component
public class GoalMapper implements Mapper<Goal, GoalDTO> {

    private static final Supplier<EntityNotFoundException> ENTITY_NOT_FOUND = EntityNotFoundException::new;

    private final GroupRepository groupRepository;

    @Autowired
    public GoalMapper(@Lazy GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }


    @Override
    public Goal dtoToEntity(GoalDTO dto) {
        Goal entity = new Goal();

        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setTargetAmount(dto.getTargetAmount());
        entity.setTargetDate(dto.getTargetDate());
        entity.setSaving(groupRepository.findById(dto.getGroupId()).orElseThrow(ENTITY_NOT_FOUND).getSaving());

        return entity;
    }

    @Override
    public GoalDTO entityToDto(Goal entity) {
        GoalDTO dto = new GoalDTO();

        dto.setDescription(entity.getDescription());
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setTargetAmount(entity.getTargetAmount());
        dto.setTargetDate(entity.getTargetDate());
        dto.setGroupId(entity.getSaving().getGroupId());

        return dto;
    }
}
