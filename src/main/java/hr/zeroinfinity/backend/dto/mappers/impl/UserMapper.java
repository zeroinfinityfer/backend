package hr.zeroinfinity.backend.dto.mappers.impl;

import hr.zeroinfinity.backend.dto.UserDTO;
import hr.zeroinfinity.backend.dto.mappers.Mapper;
import hr.zeroinfinity.backend.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements Mapper<User, UserDTO> {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapper(@Lazy PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User dtoToEntity(UserDTO dto) {
        User entity = new User();

        entity.setName(dto.getName());
        entity.setSurname(dto.getSurname());
        entity.setEmail(dto.getEmail());
        entity.setPasswordHash(passwordEncoder.encode(dto.getPassword()));
        entity.setVerified(false);
        entity.setPersonalGroupEnabled(dto.getPersonalGroupEnabled());

        return entity;
    }

    @Override
    public UserDTO entityToDto(User entity) {
        UserDTO dto = new UserDTO();

        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
        dto.setEmail(entity.getEmail());
        dto.setId(entity.getId());
        dto.setPersonalGroupEnabled(entity.getPersonalGroupEnabled());

        return dto;
    }
}
