package hr.zeroinfinity.backend.dto.mappers.impl;

import hr.zeroinfinity.backend.dto.IncomeDTO;
import hr.zeroinfinity.backend.dto.mappers.Mapper;
import hr.zeroinfinity.backend.entities.Income;
import hr.zeroinfinity.backend.repositories.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.function.Supplier;

@Component
public class IncomeMapper implements Mapper<Income, IncomeDTO> {

    private static final Supplier<EntityNotFoundException> ENTITY_NOT_FOUND = EntityNotFoundException::new;
    private final GroupRepository groupRepository;

    @Autowired
    public IncomeMapper(@Lazy GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public Income dtoToEntity(IncomeDTO dto) {
        Income entity = new Income();

        entity.setAmount(dto.getAmount());
        entity.setDescription(dto.getDescription());
        entity.setDateCreated(dto.getDateCreated());
        entity.setGroup(groupRepository.findById(dto.getGroupId()).orElseThrow(ENTITY_NOT_FOUND));

        return entity;
    }

    @Override
    public IncomeDTO entityToDto(Income entity) {
        IncomeDTO dto = new IncomeDTO();

        dto.setAmount(entity.getAmount());
        dto.setDescription(entity.getDescription());
        dto.setGroupId(entity.getGroupId());
        dto.setId(entity.getId());
        dto.setDateCreated(entity.getDateCreated());

        return dto;
    }
}
