package hr.zeroinfinity.backend.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
public class GroupAdminDTO {

    @NotNull(message = "form.basic.notempty")
    public UUID groupId;

    @NotNull(message = "form.basic.notempty")
    public UUID userId;
}
