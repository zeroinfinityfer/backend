package hr.zeroinfinity.backend.dto.jwt;

import java.io.Serializable;
import java.security.Principal;

public class UserPrincipal implements Principal, Serializable {

    private UserTokenData userTokenData;

    private String name;

    public UserPrincipal(UserTokenData userTokenData, String name) {
        this.userTokenData = userTokenData;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserTokenData getUserTokenData() {
        return userTokenData;
    }

    public void setUserTokenData(UserTokenData userTokenData) {
        this.userTokenData = userTokenData;
    }
}