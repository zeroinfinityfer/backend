package hr.zeroinfinity.backend.dto.jwt;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
public class UserTokenData {

    private UUID id;
    private String email;
    private String name;
    private String surname;
}
