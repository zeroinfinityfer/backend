package hr.zeroinfinity.backend.dto.validators;

import hr.zeroinfinity.backend.dto.UserDTO;
import hr.zeroinfinity.backend.services.UserService;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.persistence.EntityNotFoundException;
import java.util.function.IntPredicate;


@Component
public class UserDTOValidator implements Validator {

    private static final EmailValidator EMAIL_VALIDATOR = EmailValidator.getInstance();

    private static int MAX_FIELD_LENGTH = 128;
    private static int MIN_PASSWORD_SIZE = 8;

    private static final String NAME = "name";
    private static final String SURNAME = "surname";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String PERSONAL_GROUP = "personalGroupEnabled";
    private static final String CURRENCY = "currency";

    private static final IntPredicate IS_NAME_PREDICATE = c -> Character.isAlphabetic(c) || c == ' ' || c == '-';

    private static final String EMPTY_NAME = "appUserForm.name.empty";
    private static final String TOO_LONG_NAME = "appUserForm.name.tooLong";
    private static final String BAD_FORMAT_NAME = "appUserForm.name.badFormat";
    private static final String EMPTY_SURNAME = "appUserForm.surname.empty";
    private static final String TOO_LONG_SURNAME = "appUserForm.surname.tooLong";
    private static final String BAD_FORMAT_SURNAME = "appUserForm.surname.badFormat";
    private static final String EMPTY_EMAIL = "appUserForm.email.empty";
    private static final String TOO_LONG_EMAIL = "appUserForm.email.tooLong";
    private static final String BAD_FORMAT_EMAIL = "appUserForm.email.badFormat";
    private static final String EXISTING_EMAIL = "appUserForm.email.unique";
    private static final String EMPTY_PASSWORD = "appUserForm.password.empty";
    private static final String TOO_SHORT_PASSWORD = "appUserForm.password.tooShort";
    private static final String TOO_LONG_PASSWORD = "appUserForm.password.tooLong";
    private static final String EXEPTION_PERSONAL_GROUP = "appUserForm.personalGroupEnabled.badFormat";
    private static final String CURRENCY_NOT_VALID = "appUserForm.currency.notValid";

    private final UserService userService;

    @Autowired
    public UserDTOValidator(UserService userService) {
        this.userService = userService;
    }


    @Override
    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDTO user = (UserDTO) o;

        prepareFields(user);

        validateName(user, errors);
        validateSurname(user, errors);
        validateEmail(user, errors);
        validatePassword(user, errors);
        validatePersonalGroupEnabled(user, errors);
        validateCurrency(user, errors);
    }


    private void validateCurrency(UserDTO user, Errors errors) {
        if(user.getCurrency() == null || user.getCurrency().length() != 3){
            errors.rejectValue(CURRENCY, CURRENCY_NOT_VALID);
        }
    }


    /**
     * Method checks the validity of the name field.
     */
    private void validateName(UserDTO user, Errors errors) {
        String name = user.getName();

        if (name == null || name.isEmpty()) {
            errors.rejectValue(NAME, NAME, EMPTY_NAME);
        } else if (name.length() > MAX_FIELD_LENGTH) {
            errors.rejectValue(NAME, NAME, TOO_LONG_NAME);
        } else if (!name.chars().allMatch(IS_NAME_PREDICATE)) {
            errors.rejectValue(NAME, NAME, BAD_FORMAT_NAME);
        }
    }

    /**
     * Method checks the validity of the surname field.
     */
    private void validateSurname(UserDTO user, Errors errors) {
        String surname = user.getSurname();

        if (surname == null || surname.isEmpty()) {
            errors.rejectValue(SURNAME, SURNAME, EMPTY_SURNAME);
        } else if (surname.length() > MAX_FIELD_LENGTH) {
            errors.rejectValue(SURNAME, SURNAME, TOO_LONG_SURNAME);
        } else if (!surname.chars().allMatch(IS_NAME_PREDICATE)) {
            errors.rejectValue(SURNAME, SURNAME, BAD_FORMAT_SURNAME);
        }
    }

    /**
     * Method checks the validity of the email field.
     */
    private void validateEmail(UserDTO user, Errors errors) {
        String email = user.getEmail();

        if (email == null || email.isEmpty()) {
            errors.rejectValue(EMAIL, EMAIL, EMPTY_EMAIL);
        } else {
            if (!EMAIL_VALIDATOR.isValid(email)) {
                errors.rejectValue(EMAIL, EMAIL, BAD_FORMAT_EMAIL);
            } else if (email.length() > MAX_FIELD_LENGTH) {
                errors.rejectValue(EMAIL, EMAIL, TOO_LONG_EMAIL);
            } else {
                try {
                    userService.getUserByEmail(email);
                    errors.rejectValue(EMAIL, EMAIL, EXISTING_EMAIL);
                } catch (EntityNotFoundException ignorable) {
                }
            }
        }
    }

    /**
     * Method checks the validity of the password field.
     */
    private void validatePassword(UserDTO user, Errors errors) {

        String password = user.getPassword();

        if (password == null) {
            errors.rejectValue(PASSWORD, PASSWORD, EMPTY_PASSWORD);
        } else {
            if (password.length() < MIN_PASSWORD_SIZE) {
                errors.rejectValue(PASSWORD, PASSWORD, TOO_SHORT_PASSWORD);
            } else if (password.length() > MAX_FIELD_LENGTH) {
                errors.rejectValue(PASSWORD, PASSWORD, TOO_LONG_PASSWORD);
            }
        }

    }

    /**
     * Method checks the validity of the PersonalGroupEnabled.
     */
    private void validatePersonalGroupEnabled(UserDTO user, Errors errors) {
        Boolean personalGroupEnabled = user.getPersonalGroupEnabled();
        if (personalGroupEnabled == null) {
            errors.rejectValue(PERSONAL_GROUP, PERSONAL_GROUP, EXEPTION_PERSONAL_GROUP);
        }
    }

    /**
     * Changes fields that are null to empty strings and trims the fields. Fields that are checked
     * are name, surname and email.
     */
    private void prepareFields(UserDTO user) {
        user.setName(user.getName() == null ? "" : user.getName().trim());
        user.setSurname(user.getSurname() == null ? "" : user.getSurname().trim());
        user.setEmail(user.getEmail() == null ? "" : user.getEmail().trim());
    }

}
