package hr.zeroinfinity.backend.dto.validators;

import hr.zeroinfinity.backend.dto.GroupDTO;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Component
public class GroupDTOValidator implements Validator {

    private static final EmailValidator EMAIL_VALIDATOR = EmailValidator.getInstance();
    private static final int MAX_FIELD_LENGTH = 128;

    private static final String NAME = "name";
    private static final String MEMBERS = "members[%d]";
    private static final String CURRENCY = "currency";

    private static final String EMPTY_NAME = "appGroupForm.name.empty";
    private static final String TOO_LONG_NAME = "appGroupForm.name.tooLong";
    private static final String EMPTY_EMAIL = "appGroupForm.email.empty";
    private static final String TOO_LONG_EMAIL = "appGroupForm.email.tooLong";
    private static final String BAD_FORMAT_EMAIL = "appGroupForm.email.badFormat";
    private static final String CURRENCY_BAD_FORMAT = "appUserForm.currency.notValid";

    @Override
    public boolean supports(Class<?> aClass) {
        return GroupDTO.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        GroupDTO groupDTO = (GroupDTO) o;

        prepareFields(groupDTO);
        validateName(groupDTO, errors);
        validateMembers(groupDTO, errors);
        validateCurrency(groupDTO, errors);
    }

    private void validateCurrency(GroupDTO groupDTO, Errors errors) {
        if(groupDTO.getCurrency() == null || groupDTO.getCurrency().length() != 3){
            errors.rejectValue(CURRENCY, CURRENCY_BAD_FORMAT);
        }
    }

    /**
     * Method checks if name is valid.
     */
    public void validateName(GroupDTO groupDTO, Errors errors) {
        String name = groupDTO.getName();

        if (name == null || name.isEmpty()) {
            errors.rejectValue(NAME, NAME, EMPTY_NAME);
        } else if (name.length() > MAX_FIELD_LENGTH) {
            errors.rejectValue(NAME, NAME, TOO_LONG_NAME);
        }
    }

    /**
     * Method checks if members email is valid email.
     */

    public void validateMembers(GroupDTO groupDTO, Errors errors) {
        List<String> members = groupDTO.getInvitationEmails();
        HashSet<String> uniqueMembers = new HashSet<>();

        for (int i = 0; i<members.size(); i++) {
            String email = members.get(i);
            if (email == null) continue;

            email = email.trim();
            if (email.isEmpty()) {
                errors.rejectValue(String.format(MEMBERS,i), String.format(MEMBERS,i), EMPTY_EMAIL);
            } else if (!EMAIL_VALIDATOR.isValid(email)) {
                errors.rejectValue(String.format(MEMBERS,i), String.format(MEMBERS,i), BAD_FORMAT_EMAIL);
            } else if (email.length() > 128) {
                errors.rejectValue(String.format(MEMBERS,i), String.format(MEMBERS,i), TOO_LONG_EMAIL);
            }

            uniqueMembers.add(email);
        }


        groupDTO.setInvitationEmails(new ArrayList<>(uniqueMembers));

    }

    private void prepareFields(GroupDTO groupDTO) {
        groupDTO.setName(groupDTO.getName() == null ? "" : groupDTO.getName().trim());
        groupDTO.setDescription(groupDTO.getDescription() == null ? "" : groupDTO.getDescription().trim());

        if (groupDTO.getInvitationEmails() == null) groupDTO.setInvitationEmails(new ArrayList<>());
    }
}
