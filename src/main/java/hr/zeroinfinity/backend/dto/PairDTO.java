package hr.zeroinfinity.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class PairDTO implements Serializable {
    private static final long serialVersionUID = -6817998951227513891L;

    private String date;

    private Double value;
}
