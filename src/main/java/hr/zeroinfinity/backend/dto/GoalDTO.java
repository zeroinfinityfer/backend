package hr.zeroinfinity.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
public class GoalDTO implements Serializable {

    private static final long serialVersionUID = -3898743063767031928L;

    private Long id;

    @NotNull(message = "form.basic.notempty")
    @Size(max = 128, message = "form.basic.inputTooLong")
    private String name;

    private String description;

    @NotNull(message = "form.basic.notempty")
    @Min(value = 0, message = "form.basic.greaterThanZero")
    private BigDecimal targetAmount;

    @NotNull(message = "form.basic.notempty")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UUID groupId;

    @NotNull(message = "form.basic.notempty")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date targetDate;
}
