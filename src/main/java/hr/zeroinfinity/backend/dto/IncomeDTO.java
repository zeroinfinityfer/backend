package hr.zeroinfinity.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
public class IncomeDTO implements Serializable {
    private static final long serialVersionUID = -3063740826013558309L;

    private Long id;

    @NotNull(message = "form.basic.notempty")
    @Min(value = 0, message = "form.basic.greaterThanZero")
    private BigDecimal amount;

    private String description;

    @NotNull(message = "form.basic.notempty")
    private UUID groupId;

    @NotNull(message = "form.basic.notempty")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dateCreated;
}
