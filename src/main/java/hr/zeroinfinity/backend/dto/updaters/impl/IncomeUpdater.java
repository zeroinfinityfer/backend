package hr.zeroinfinity.backend.dto.updaters.impl;

import hr.zeroinfinity.backend.dto.IncomeDTO;
import hr.zeroinfinity.backend.dto.updaters.EntityUpdater;
import hr.zeroinfinity.backend.entities.Income;
import org.springframework.stereotype.Component;

@Component
public class IncomeUpdater implements EntityUpdater<Income, IncomeDTO> {

    @Override
    public Income updateEntity(Income entity, IncomeDTO dto) {
        return fillData(entity, dto);
    }

    private Income fillData(Income entity, IncomeDTO dto) {
        entity.setAmount(dto.getAmount());
        entity.setDateCreated(dto.getDateCreated());
        entity.setDescription(dto.getDescription());

        return entity;
    }
}
