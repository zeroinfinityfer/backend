package hr.zeroinfinity.backend.dto.updaters.impl;

import hr.zeroinfinity.backend.dto.GoalDTO;
import hr.zeroinfinity.backend.dto.updaters.EntityUpdater;
import hr.zeroinfinity.backend.entities.Goal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GoalUpdater implements EntityUpdater<Goal, GoalDTO> {


    @Autowired
    public GoalUpdater() {

    }

    @Override
    public Goal updateEntity(Goal entity, GoalDTO dto) {
        return fillData(entity, dto);
    }

    private Goal fillData(Goal entity, GoalDTO dto) {
        entity.setDescription(dto.getDescription());
        entity.setName(dto.getName());
        entity.setTargetAmount(dto.getTargetAmount());
        entity.setTargetDate(dto.getTargetDate());

        return entity;
    }
}
