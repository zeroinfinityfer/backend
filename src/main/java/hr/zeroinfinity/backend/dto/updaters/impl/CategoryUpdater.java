package hr.zeroinfinity.backend.dto.updaters.impl;

import hr.zeroinfinity.backend.dto.CategoryDTO;
import hr.zeroinfinity.backend.dto.updaters.EntityUpdater;
import hr.zeroinfinity.backend.entities.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryUpdater implements EntityUpdater<Category, CategoryDTO> {

    @Override
    public Category updateEntity(Category entity, CategoryDTO dto) {
        return fillData(entity, dto);
    }

    private Category fillData(Category entity, CategoryDTO dto) {
        entity.setName(dto.getName());

        return entity;
    }
}
