package hr.zeroinfinity.backend.dto.updaters.impl;

import hr.zeroinfinity.backend.dto.GroupDTO;
import hr.zeroinfinity.backend.dto.updaters.EntityUpdater;
import hr.zeroinfinity.backend.entities.Group;
import org.springframework.stereotype.Component;

@Component
public class GroupUpdater implements EntityUpdater<Group, GroupDTO> {

    @Override
    public Group updateEntity(Group entity, GroupDTO dto) {
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());

        return entity;
    }
}
