package hr.zeroinfinity.backend.dto.updaters;

/**
 * Interface describing the object which performs an update over an entity.
 *
 * @param <E> entity type
 * @param <D> dto type
 */
public interface EntityUpdater<E, D> {

    /**
     * Updates entity with the information provided in data transfer object.
     *
     * @param entity orm entity
     * @param dto    data transfer object
     * @return updated entity
     * @throws UnsupportedOperationException if method is not supported by the implementation
     */
    E updateEntity(E entity, D dto);
}