package hr.zeroinfinity.backend.dto.updaters.impl;

import hr.zeroinfinity.backend.dto.ExpenseDTO;
import hr.zeroinfinity.backend.dto.updaters.EntityUpdater;
import hr.zeroinfinity.backend.entities.Expense;
import hr.zeroinfinity.backend.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.function.Supplier;

@Component
public class ExpenseUpdater implements EntityUpdater<Expense, ExpenseDTO> {


    private static final Supplier<EntityNotFoundException> ENTITY_NOT_FOUND_EXCEPTION_SUPPLIER = EntityNotFoundException::new;
    private final CategoryRepository categoryRepository;

    @Autowired
    public ExpenseUpdater(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public Expense updateEntity(Expense entity, ExpenseDTO dto) {
        return fillData(entity, dto);
    }

    private Expense fillData(Expense entity, ExpenseDTO dto) {
        entity.setAmount(dto.getAmount());
        entity.setCategory(categoryRepository.findById(dto.getCategoryId()).orElseThrow(ENTITY_NOT_FOUND_EXCEPTION_SUPPLIER));
        entity.setDateCreated(dto.getDateCreated());
        entity.setDescription(dto.getDescription());

        return entity;
    }
}
