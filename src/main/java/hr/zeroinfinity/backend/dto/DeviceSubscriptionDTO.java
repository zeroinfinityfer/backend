package hr.zeroinfinity.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@AllArgsConstructor
@Data
public class DeviceSubscriptionDTO implements Serializable {
    private static final long serialVersionUID = -2619288036113611423L;

    private String oldToken;

    @NotNull(message = "form.basic.notempty")
    private String newToken;
}
