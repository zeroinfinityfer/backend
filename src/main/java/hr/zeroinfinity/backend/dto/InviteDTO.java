package hr.zeroinfinity.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
public class InviteDTO {

    private Long id;

    private UUID groupId;

    private String email;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date createdAt;

    private String groupName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UserDTO invitedBy;
}
