package hr.zeroinfinity.backend.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
public class SavingEntryDTO implements Serializable {
    private static final long serialVersionUID = -3964497631893404278L;

    private Long id;

    @NotNull(message = "form.basic.notempty")
    private UUID groupId;

    @NotNull(message = "form.basic.notempty")
    @Min(value = 0, message = "form.basic.greaterThanZero")
    private BigDecimal amountAdded;

    private BigDecimal currentAmount;

    @NotNull(message = "form.basic.notempty")
    private Date dateCreated;
}
