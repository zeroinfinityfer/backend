package hr.zeroinfinity.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDTO implements Serializable {

    private static final long serialVersionUID = -5707614927988241302L;

    private Long id;

    @NotEmpty(message = "form.basic.notempty")
    @Size(max = 128, message = "form.basic.inputTooLong")
    private String name;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private UUID groupId;
}
