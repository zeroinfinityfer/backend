package hr.zeroinfinity.backend.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
public class GroupInviteDTO implements Serializable {
    private static final long serialVersionUID = 8903213795680526620L;

    @NotNull(message = "form.basic.notempty")
    private UUID id;

    @NotEmpty(message = "form.basic.notempty")
    private List<@Email(message = "appGroupForm.email.badFormat") String> members;
}
