package hr.zeroinfinity.backend.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
public class GroupMemberRemoveDTO {

    @NotNull(message = "form.basic.notempty")
    private UUID userId;

    @NotNull(message = "form.basic.notempty")
    private UUID groupId;
}
