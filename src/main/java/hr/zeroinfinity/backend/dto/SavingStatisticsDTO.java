package hr.zeroinfinity.backend.dto;

import lombok.Data;

import java.util.List;

@Data
public class SavingStatisticsDTO {

    private List<PairDTO> savings;

    private List<GoalDTO> goals;
}
