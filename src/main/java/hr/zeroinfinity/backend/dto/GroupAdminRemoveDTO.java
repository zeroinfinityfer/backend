package hr.zeroinfinity.backend.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
public class GroupAdminRemoveDTO {

    @NotNull(message = "form.basic.notempty")
    private UUID groupId;
}
