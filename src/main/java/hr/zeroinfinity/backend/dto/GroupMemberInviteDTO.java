package hr.zeroinfinity.backend.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
public class GroupMemberInviteDTO {

    @NotNull(message = "form.basic.notempty")
    private UUID id;

    @NotNull(message = "form.basic.notempty")
    @Email(message = "appUserForm.email.badFormat")
    private String memberEmail;
}
