package hr.zeroinfinity.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DailyStatisticsDTO implements Serializable {
    private static final long serialVersionUID = -1713576164362101208L;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<IncomeDTO> incomes;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ExpenseDTO> expenses;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<GoalDTO> goals;
}
