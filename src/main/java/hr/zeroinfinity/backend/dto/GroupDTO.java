package hr.zeroinfinity.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
public class GroupDTO implements Serializable {
    private static final long serialVersionUID = -2432583920835065421L;

    private UUID id;

    private String name;

    private String description;

    private String currency;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<UserDTO> members;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<UserDTO> admins;

    private Boolean personal;

    private BigDecimal balance;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<String> invitationEmails;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<CategoryDTO> categories;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal savingsAmount;
}
