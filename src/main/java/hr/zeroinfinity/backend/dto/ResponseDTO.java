package hr.zeroinfinity.backend.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
public class ResponseDTO implements Serializable {
    private static final long serialVersionUID = -7806523070669300374L;

    private boolean valid = true;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, String> errors = new HashMap<>();

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object entityId;

    public void addError(String name, String description) {
        valid = false;
        errors.put(name, description);
    }

}
