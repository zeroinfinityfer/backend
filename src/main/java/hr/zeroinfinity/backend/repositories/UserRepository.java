package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByEmail(String email);

    Boolean existsByEmail(String email);

    /**
     * Checks if user with given @param userEmail exists in group @param groupId
     */
    @Query(value = "SELECT CASE WHEN count(user.id)>0 THEN true ELSE false END " +
            "FROM User as user INNER JOIN user.groups as groups " +
            "WHERE groups.id= :groupId " +
            "AND user.email= :userEmail")
    Boolean existByGroupMemberEmail(@Param("groupId") UUID id, @Param("userEmail") String email);

    /**
     * Checks if user with given @param userId exists in group @param groupId
     */
    @Query(value = "SELECT CASE WHEN count(user.id)>0 THEN true ELSE false END " +
            "FROM User as user INNER JOIN user.groups as groups " +
            "WHERE groups.id= :groupId " +
            "AND user.id= :userId")
    Boolean existByGroupMemberId(@Param("groupId") UUID groupId, @Param("userId") UUID userId);

}
