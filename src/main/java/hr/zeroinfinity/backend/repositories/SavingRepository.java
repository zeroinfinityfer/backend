package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.Saving;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SavingRepository extends JpaRepository<Saving, Long> {
    Saving findByGroupId(UUID groupId);
}
