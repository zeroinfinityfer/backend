package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.Income;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface IncomeRepository extends JpaRepository<Income, Long> {
    Page<Income> findAllByGroupId(UUID groupId, Pageable pageable);

    @Query("select i from Income i where i.groupId = (:groupId) and i.dateCreated > (:fromDate) and i.dateCreated <= (:toDate)")
    List<Income> findAllInRange(@Param("groupId") UUID groupId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    List<Income> findAllByGroupIdAndDateCreated(UUID groupId, Date dateCreated);
}
