package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.Goal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface GoalRepository extends JpaRepository<Goal, Long> {
    Page<Goal> findAllBySavingId(Long savingId, Pageable pageable);

    List<Goal> findAllBySavingId(Long savingId);

    List<Goal> findAllBySavingIdAndTargetDate(Long savingId, Date targetDate);

    @Query("select g from Goal g where g.savingId = (:savingId) and g.targetDate > (:fromDate) and g.targetDate <= (:toDate)")
    List<Goal> findAllInRange(Long savingId, Date fromDate, Date toDate);
}
