package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
    Device findFirstByDeviceCode(String deviceCode);
}
