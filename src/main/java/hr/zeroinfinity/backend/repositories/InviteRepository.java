package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.Invite;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface InviteRepository extends JpaRepository<Invite, Long> {

    List<Invite> findByEmail(String email);

    Boolean existsByEmailAndGroupId(String email, UUID groupId);
}
