package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.Expense;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long> {
    Page<Expense> findAllByGroupId(UUID groupId, Pageable pageable);

    @Query("select e from Expense e where e.groupId = (:groupId) and e.dateCreated > (:fromDate) and e.dateCreated <= (:toDate)")
    List<Expense> findAllInRange(@Param("groupId") UUID groupId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    List<Expense> findAllByGroupIdAndDateCreated(UUID groupId, Date dateCreated);
}