package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.Registration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RegistrationRepository extends JpaRepository<Registration, Long> {
    Optional<Registration> findByUserId(UUID userId);

    List<Registration> findAllByCreatedAtBefore(Date date);
}
