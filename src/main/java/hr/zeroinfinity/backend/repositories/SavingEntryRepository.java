package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.SavingEntry;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface SavingEntryRepository extends JpaRepository<SavingEntry, Long> {

    @Query("select e from SavingEntry e where e.savingId = (:savingId) and e.dateCreated > (:fromDate) and e.dateCreated <= (:toDate)")
    List<SavingEntry> findAllInRange(@Param("savingId") Long savingId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

    @Query("select e from SavingEntry e where e.savingId = (:savingId) and e.dateCreated <= (:date) order by e.dateCreated desc, e.id desc")
    List<SavingEntry> findByDateCustom(@Param("date") Date date,@Param("savingId") Long savingsId, Pageable pageable);

    default Optional<SavingEntry> findByDateCustom(Date date, Long savingId){
        List<SavingEntry> savingEntries = findByDateCustom(date, savingId, PageRequest.of(0, 1));

        if(savingEntries.size() > 0){
            return Optional.of(savingEntries.get(0));
        } else {
            return Optional.empty();
        }
    }
}
