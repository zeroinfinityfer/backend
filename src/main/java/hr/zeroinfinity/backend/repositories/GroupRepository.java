package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface GroupRepository extends JpaRepository<Group, UUID> {


    @Query("SELECT g FROM Group g JOIN FETCH g.admins WHERE g.id = (:id)")
    Group findByIdAndFetchAttachedAdmins(@Param("id") UUID id);

    @Query("SELECT g FROM Group g JOIN FETCH g.members WHERE g.id = (:id)")
    Group findByIdAndFetchAttachedMembers(@Param("id") UUID id);


    /**
     * Checks if user with given @param userId is admin in group with @param groupId
     */
    @Query(value = "SELECT CASE WHEN count(g.id)>0 THEN true ELSE false END " +
            "FROM Group as g INNER JOIN g.admins as admins " +
            "WHERE g.id = :groupId " +
            "AND admins.id= :userId")
    Boolean existByGroupAdminId(@Param("groupId") UUID groupId, @Param("userId") UUID userID);


    @Query(value = "SELECT count(admins.id) " +
            "FROM Group as g INNER JOIN g.admins as admins " +
            "WHERE g.id = :groupId ")
    int countGroupAdmins(@Param("groupId") UUID groupId);

}
