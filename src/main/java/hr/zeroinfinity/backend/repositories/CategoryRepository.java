package hr.zeroinfinity.backend.repositories;

import hr.zeroinfinity.backend.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query(value = "select cat from Category cat where cat.group.id = (:groupId)")
    List<Category> findAllByGroup(@Param("groupId") UUID groupId);

    @Query(value = "select cat from Category cat where cat.group.id = null")
    List<Category> findDefault();
}
